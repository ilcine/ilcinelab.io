 
export default {
  name: 'Content',
  props: {
    modelValues: { type: Object, required: true },
  },
  setup(props,{emit}) {      
  
      const {onMounted, ref, inject } = Vue;
      const converter = new showdown.Converter();		// for md to html 
      const serverdataMD = ref(null);
      const pageName = ref(props.modelValues);

    onMounted(() => { 

      fetch('./data/' + pageName.value.pageLink1 + '/' +  pageName.value.pageLink2 )
          .then(async r => { 
            const data = await r.text();
            serverdataMD.value = converter.makeHtml(data); // for md to html

           }); 
        
    })

    return {serverdataMD, pageName }

  
  },

  // v-html ham html yi de gösterir  
  template: `
        <div class="markdown-body">   
        <h3> page: {{ pageName.pageLink1}}  </h3>
          
          <span v-html="serverdataMD"></span>
        </div>
    `,
};

export default {
    name: 'Laravel',
     
    props: {
      modelValues: { type: Object, required: true },
    },
    
    emits: ['modelValuesEmit'],

    setup(props,{emit}) {
      const {onMounted, ref} = Vue;    
      const textToJson = ref({})
      const pageName = ref(props.modelValues.navPage);
      console.log('Page:', pageName.value)
      
      onMounted(() => {  
             /*
            fetch('./data/' + props.modelValues.navPage + 'Content.txt')
            .then(async response => response.text())
            .then(data => {      
              textToJson.value =  text2json(data.replace(/(\r)/gm,""));
              //console.log('json:',textToJson.value)
              console.log('json:',data)
             });
             */
           fetch('./data/' + props.modelValues.navPage + 'Content.txt')
            .then(async r => { 
              const data = await r.text();
              textToJson.value =  await text2json(data.replace(/(\r)/gm,""));
              //console.log('json:',textToJson.value)
              //console.log('json:',data)
             });
             
             
      })
      
      //emit message to parent (and reset after 3 seconds)
      function returnEmit(item) {
        emit('modelValuesEmit', {
            navPage:"Content", 
            pageLink1: props.modelValues.navPage, 
            pageLink2: item
            });
      }
        
      return {  textToJson,returnEmit,pageName  }
      
      // data virgül formatlı oluşturulmuştu bunu "json" a dönüştürüyorum.
      function text2json(str) {

	      // --------------------convert text to json begin--------------------------------------------------
				// *** first line end delimiter by "\n" (newline)
				// *** elements in other rows are separated by  ' ' (space)
				// https://stackoverflow.com/questions/36120265/how-to-convert-text-file-to-json-in-javascript
				//
				//var str = 'id      nm              lat         lon         countryCode\n819827  Razvilka        55.591667   37.740833   RU\n524901  Moscow          55.752220   37.615555   RU\n1271881 Firozpur        27.799999   76.949997   IN';
			  //	var str = result.data;
				
				//var cells = str.split('\n').map(function (el) { return el.split(/\s+/); }); // space= (/\s+/) ; delimiter " "
				var cells = str.split('\n').map(function (el) { return el.split(/\s*,\s*/); }); // space= (/\s+/) delimiter ","

				var headings = cells.shift();
				var out = cells.map(function (el) {
					var obj = {};
					for (var i = 0, l = el.length; i < l; i++) {
						obj[headings[i]] = isNaN(Number(el[i])) ? el[i] : +el[i];
					}
					
					return obj;
				});
				//console.log(JSON.stringify(out, null, 2));
				//console.log(123);
        // -------------------convert text to json end------------------------------------------------------------
        //console.log('out: ',out);
				return out;
			};
      
    },
    
    template: `
        <div>
       			<div id="floated" style="padding:5px;" >
             <img src="img/LaravelNotes.png" alt="Vue" style="max-width:5%; height:auto;">
             {{ pageName }} Notes
            </div>
           

           
                       
            <div style="columns: 2; -webkit-columns: 2; -moz-columns: 2;">
              <div v-for="(item,index) in textToJson" key="index" style="padding:3px;" >              
                 <div class="box" v-on:click="returnEmit(item.activeFiles)">
                    <div> {{ (index +1 ).toLocaleString(undefined, {minimumIntegerDigits: 2}) }} &nbsp;</div>
                    <div> {{ item.title }} </div>
                 </div>
               </div>     
            </div>  
           
           
        </div>
    `,
  };
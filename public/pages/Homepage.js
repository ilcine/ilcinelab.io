
export default {
  
    name: 'Homepage',
    props: {
          modelValues: { type: Object, required: true }, 
        },
    emits: ['modelValuesEmit'], 
    
    setup(props,{emit}) {
     const {onMounted, ref } = Vue;
     console.log('ActivePageNav:', props.modelValues.navPage)
     const linkName = ref([{"name":"Linux"},
                           {"name":"Laravel"},
                           {"name":"Ubuntu"},
                           {"name":"Vue"},
                           {"name":"Projects"},
                           {"name":"Network"}])
    function returnEmit(item) {
        emit('modelValuesEmit', {
            navPage: item, 
            pageLink1: null, 
            pageLink2: null
            });
      }
      
    const dataArray = [
      
        { Laravel: 
             `Laravel is an open source PHP framework with MVC structure. 
             "Object Oriented Programming (OOP)"; 
             "Object Relational Mapping (ORM)" 
             in Object relationship with DB; 
             "Composer" as dependency management; 
             "Artisan" is used for configuration. 
             In these notes, the subjects 
             that will be needed about Laravel are explained with examples. `,  
          Linux: 
           `The most basic code of computers is
            kernel (kernel) with GNU General Public License
            developed and presented with a free use right
            operating system software project.
            There are many linux versions. (Debian, Redhat, Centos, Ubuntu, Suse, Fedora etc.).
            Command codes with small changes
            It's like Unix systems. This documentation was taken as Ubuntu.`, 
          Ubuntu: 
           `Ubuntu is an open source 
            and free operating system developed on the Linux kernel. 
            These notes have been tested on Ubuntu 16.04.
           `,
           
          Vue: 
            `VueJS is a framework that uses javaScript features. 
            Provides coding for front-end programmers 
            with interactive Web content. 
            Immediately detects changes in CSS or HTML 
            and applies the page without refresh.`,
            
          Projects:
            `projeler: Accounting, dns server, ldap server `, 
            
          Network: 
               `The internet is defined as a global network of linked computers, 
               servers, phones, and smart appliances that communicate 
               with each other using the transmission control protocol (TCP) standard 
               to enable the fast exchange of information and files, along with other types of services.
                 `
              }
      ];
      
      //const activePage = props.modelValues.navPage

      return { dataArray, linkName, returnEmit };

    },
 
    template: `
        

      <div v-for="item, index in linkName" key="item.name">
          <div v-on:click="returnEmit(item.name)">
              <span style="display: block;" > <!-- başlık alttaki açıklama ile birlikte olmasi icin block kullanıldı -->
              {{ item.name }} Notes
              <span>
               
              <span style="display: block;"  > 
                <section  style="cursor:pointer" >
                  <img :src = "'img/' + item.name + 'Notes.png'" style="max-width: 16%;	height: auto; border-radiusX: 50%;padding:5px;"> 
          
                  <p class="textfade">
                   <span v-html="dataArray[0][item.name]" class="markdown-body" ></span> 
                  </p>
                </section>
              </span>
              
          </div> 
      </div>


    `,
};
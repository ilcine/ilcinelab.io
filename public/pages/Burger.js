
export default {

  name:'Burger',
  props: {
          modelValues: { type: Object, required: true }, 
        },
  emits: ['modelValuesEmit'], 
    
    setup(props,{emit}) {
     const {onMounted, ref } = Vue;
     console.log('ActivePageNav:', props.modelValues.navPage)
     const linkName = ref([{"name":"Linux"},
                           {"name":"Laravel"},
                           {"name":"Ubuntu"},
                           {"name":"Vue"},
                           {"name":"Projects"},
                           {"name":"Network"}])

    const isOpenBurger = ref(false);
    
    function returnEmit(item) {
        emit('modelValuesEmit', {
            navPage: item, 
            pageLink1: null, 
            pageLink2: null
            });
    }
      
    onMounted(() => { 
    });

    return { 
      isOpenBurger,
      closeModals,returnEmit,linkName
    }
    
    function closeModals() {
      isOpenBurger.value = !isOpenBurger.value; 
      console.log('emit ten geldi',isOpenBurger.value )
    };
 
  }, 
  
  template: ` 
    <div>
    <!-- this burger ikon -->
    <nav title="Menu" @click="isOpenBurger = !isOpenBurger;" >
      <div style="cursor:pointer; padding:0 0 0 10px;" >
        <div class="burgerDiv"></div>
        <div class="burgerDiv"></div>
        <div class="burgerDiv"></div>
      </div> 
    </nav>
     
       
    <!-- burger enable ise açılacak -->    <!-- burger icon = &#9776;  --> 
    <transition name="modal">
      <!--div v-if="isOpenBurger && $store.state.login" --> 
      <div v-if="isOpenBurger"> 
        <div class="overlay" @click.self="closeModals()" >
          <div class="modal">
          
            <!-- Header --> 
            <div id="headerWrap">
               <label style="color:white" >Ilcine Page</label>
               <button class="Xbutton" @click="closeModals()"> X </button>
            </div>  

            <!-- linkler -->  
            <section style="padding: 4px 4px 4px 5px; height:100%">

              
              <!-- div class="routerLink" v-on:click="returnEmit('Linux'); isOpenBurger = !isOpenBurger;"  > 
                linux
              </div -->
              
                  <div v-for="item, index in linkName" key="item.name"  class="routerLink" >
                    <div v-on:click="returnEmit(item.name); isOpenBurger = !isOpenBurger; ">
                      {{ item.name }}
                    </div> 
                  </div>            

            </section>  <!-- linkler sonu -->
 
          </div>
        </div> 
      </div>
      
    </transition>

  </div>
  `
 
};


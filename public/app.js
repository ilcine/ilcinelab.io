
import Empty from './pages/Empty.js'
import * as pages from './pages/index.js' // Linux,Laravel,Ubuntu,Vue,Projects,Network,Content,Homepage

        
export default {
    
    name: 'App',
    
    components: Object.assign({Empty}, pages),

    setup() {
         
      const {watchEffect, onMounted, ref, reactive, provide } = Vue;
      //console.log('pages:',pages, ' lnx',pages.Linux.name)
      
      const linkName = ref([{"name":"Linux"},
                            {"name":"Laravel"},
                            {"name":"Ubuntu"},
                            {"name":"Vue"},
                            {"name":"Projects"},
                            {"name":"Network"} ])
      const pageProps = ref({});
      



      getQueryString()
      
      // Backspace yapılınca "page.value" ye atama yapar  
      window.onpopstate = function() {         
        pageProps.value.navPage = "Homepage";
        //console.log('runBackspace')
      };  
        
      function getQueryString() {
        // https://www.sitepoint.com/get-url-parameters-with-javascript/
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);

        pageProps.value.navPage = urlParams.get('p') || 'Homepage';
        pageProps.value.pageLink1 = urlParams.get('dty1') || null;
        pageProps.value.pageLink2 = urlParams.get('dty2') || null;
          
      }

      //url management
      watchEffect(() => {
        const url = '?p=' + pageProps.value.navPage + '&dty1=' + pageProps.value.pageLink1 + '&dty2=' + pageProps.value.pageLink2;
        window.history.pushState({url: url}, '', url); // browser url sine YAZAR.
      }) 

      //show message from child component from  "modelValuesEmit"
      function showProps(fromChild) {

        pageProps.value.navPage = fromChild.navPage;
        pageProps.value.pageLink1 = fromChild.pageLink1;
        pageProps.value.pageLink2 = fromChild.pageLink2;  
 
      }
      
      function goPage(pageName) {
         //console.log('goBack:',pageName)
         pageProps.value.navPage = pageName;
         pageProps.value.pageLink1 = null;
         pageProps.value.pageLink2 = null;
      }
        
      return {pages, linkName, pageProps, showProps,  goPage,getQueryString }
      
    },

    template: `
    
      <!-- ------------ 1) Başlık--------------------------------------------- --> 
      <!-- header>								
 
           
      </header -->  
      
            <div id="headerWrap" style="background-color: #5bc0de;"  >
                <Burger  
                   @modelValuesEmit=showProps
                   :model-values="pageProps" 
                 />
               
                <span>
                    <img src = "img/emr.jpg" style="max-width: 6%; height: auto; border-radius: 50%;vertical-align:middle">
                    İlçine Project Page
                </span>
                 
                <div>  </div>
            </div> 
      
        <!-- Nav------pages(Linux,Laravel,Ubuntu,Vue,Projects,Content)------ -->
              
         <div style="display: flex; justify-content: center;  background-color: grey; padding:10px "  >
                <button id="two" v-on:click="goPage('Homepage')"> Home </button>
                <span v-for="item, index in linkName" key="item.name">
                      <button id="two"  v-on:click="goPage(item.name)" 
                      
                      >
                          {{ item.name }}  
                      </button>
                </span>              
             
          </div>  
 
 
      <!-- ----------- 3) First page description----(if Homepage)--------- --> 
      
      <main v-if="(pageProps.navPage == 'Homepage')" >
        <component 
             :is="'Homepage'" 
             @modelValuesEmit=showProps
             :model-values="pageProps" 
        />
      </main>
      
      <!-- ---------- 4) İçerik componenti--(page ye göre aktif olur)--2.sayfa------------- --> 
      <div id="content" style="padding: 10px;"  v-if="(pageProps.navPage != 'Homepage')"  >
        <component 
            :is="pageProps.navPage || 'Empty'" 
            :model-values="pageProps"   
            @modelValuesEmit=showProps 
            />
         
      </div>
 
      <!-- -----------5) Bottom ---------------------------- -->
      <div class="Content-box-gray"  style="padding-top:15px;">
        
        <div class="Content">
            <span>	&nbsp;					 
            <img src = "img/gitlab.svg" style="max-width: 6%;height: auto; border-radius: 50%;vertical-align:middle">
            &nbsp; <a href="https://gitlab.com/ilcine/">emr's Gitlab page</a> 
            </span>
        </div>
      </div>
      
      <div style="font-size: 15px;">&nbsp;	
        Written by <a href="mailto:emr@uludag.edu.tr">emr@uludag.edu.tr</a>; Bursa/TR<br> 
      </div>
      <br>  
    `,
  };

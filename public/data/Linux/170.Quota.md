## QUOTA ( kullanıcılara disk kotası dağıtır)

> apt install quoata ile kurulur; `/etc/fstab` dosyasına tanım yapılır, 

* ör:/home dizinine kota verilecekse fstab dosyasında `/home defaults,usrquota,noexec` yetki veya yetkiler verilir. veya tüm sisteme kota tanımlanır. Ör: 
* `/dev/mapper/vg_server1-lv_root / ext4 defaults,usrjquota=aquota.user,grpjquota=aquota.group,jqfmt=vfsv0  1 1`
* `quotacheck -cug /home`   # bir kere yapılır db yaratir 
* `quotacheck -avugm` # kotaları check eder.
* `quotaon -avug` # kotayı aktif hele getirir.
* `quotaoff -avug` # kota işlemi durur.
* `setquota -a emr 10000 10000 0 0` # emr kullanıcısına 10mb kota tanımlar
* `repquota -a` # kota hakkında rapor verir

> not: kota /home ye atandı ise home dizininde `quota.user` dosyası oluşur.

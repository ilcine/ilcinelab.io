## Uniq ve sort örnekleri


### `cat abc.txt` dosyası için örnek

```
1 3
02 4
3 1
5 2
4 5
```

* `sort abc.txt` # abc.txt dosya içeriğini (ascending) artan sırada sıralar, `sort --help` tüm parametreleri gösterir, `>` ile dosyaya yönlendirilir.
* `sort -k 1 -n abc.txt` # default; küçükten büyüğe
* `sort -k 1 -nr abc.txt` # 1. kolona göre (r)büyükten küçüğe, number formatlı sıralar
* `sort -k 2 -nr abc.txt` # 2. kolona göre (r)büyükten küçğe, number formatlı sıralır. 

### `cat xyz.txt` dosyası için örnek

```
Bir
Bir
İki
Dört
Üç
```
* `cat xyz.txt| uniq` # Bir,İki,Dört,Üç ü 4 satır olarak gösterir Bir ifadesini tek gösterir.
* `cat xyz.txt |uniq -c` # Kaç tane uniq sayinin oldugunu bulur. (2 Bir) (1:iki) (1:Dört) (1:Üç)
* `sort xyz.txt |uniq -c | sort -nr |more` # uniq (biricik) olanları, ve kac tane olduğunu, ve numeric olarak sıralar. (2 Bir) (1:Üç) (1:iki) (1:Dört)
* `cat xyz.txt | uniq -u` # Sadece bir tek kaydı olan bulur.(iki) (Dört) (Üç)

### ex1  // 2.kolondan 3.kolona kadar olan anahtar // ayıraç: -t, or  --field-separator=,

sort.data
```
a,Ali,15
z,Ali,13
g,Ali,38
m,Murat,18
t,Zahide,18
```

`sort -t, --key=2 --key=3 sort.data`  or `sort -k2,3 sort.data`

```
z,Ali,13
a,Ali,15
g,Ali,38
m,Murat,18
t,Zahide,18
```

### ex 3: ikinci alanın ikinci karakterinden itibaren sırala . or Ali nin "li" sinden başla gibi 

sort -t, -k2.2 sort.data
```
t,Zahide,19
z,Ali,13
a,Ali,15
g,Ali,38
m,Murat,18
```

ex4: ilk anahtar 3 kolon ikinci anahtar 2 kolonun tersi (r) 

`sort -t,  -k3 -k2r sort.data`
```
z,Ali,13
a,Ali,15
t,Zahide,18
m,Murat,18
g,Ali,38
```

###  ex 4: number leri 0 dan başlayarak ve kolonları kendi alanlarında sınırlamasını yaparak ",2" or "",3" 
```
41275,9,20
41855,9,01
41855,9,2
41275,10,1
41359,10,0
```
`sort -t,  -k2n,2  -k3n,3 sort.data`

```
41855,9,01
41275,9,2
41855,9,20
41359,10,0
41275,10,1
```

### notes

(-n) number format 001 or 01 sort.
(-r) reverse





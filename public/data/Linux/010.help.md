## help 

Komutların sonuna `--help` yazıldığında komutla ilgili kısa açıklamalar `-`  `--` ile başlayan argümanlar la listelenir. 

* `ls --help` ls in kısa açıklaması listelenir; Sayfa sayfa listeleme için  `ls --help|more` kullan; `space` Tuşu ile devamını gör
* `ls -l` ile dosya/dizinleri long formatta gösterir.
* `ls -laR` # l:<long format> geniş format,  a:<all> tüm dosyalar, R:<alt dizinleri göster> anlamındadır.

> Komut listesini kısa yoldan görmek için `<tab><tab>` kullan; ör: l ile başlayan komutları görmek için `l<tab><tab>` kullan. Yazmaya başladığınız birçok komutu `<tab><tab>` tuşu ile tamamlayabilirsiniz. 


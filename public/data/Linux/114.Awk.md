﻿# AWK Komutu

[awk](https://www.geeksforgeeks.org/awk-command-unixlinux-examples/)

## EX1:

```
START
abc
def
START
xyz
klm
def
```

awk '/START/{x=$0;next}{print x" ~ "$0;}' file

```
START ~ abc
START ~ def
START ~ xyz
START ~ klm
START ~ def
```

## EX 2:
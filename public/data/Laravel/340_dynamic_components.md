# Dynamic Component

## 1) php artisan make:component Test

*Artisan creates two files*

* 1.1) app\View\Components\Test.php 

```php
<?php
namespace App\View\Components;
use Illuminate\View\Component;

class Test extends Component
{

  public $title; // add
  public function __construct($title)  // add
  {
	$this->title = $title;  // add
  }

  public function render()
  {
     return view('components.test');
  }
}
```
* 1.2)  resources/views/components/test.blade.php

```
<div> 
  <h3> {{$ title}} </h3>  
</div>
```

## 2) php artisan make:controller TestController

```
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    //
	public function index()
    {
        $title = "This is Title";

        return view('testDyn', compact('title')); 
    }	
		
}
```

## 3)  resources/views/testDyn.blade.php

`<x-test :title="$title"/>`


## 4) in routes/web.php
Route::get('/test', [App\Http\Controllers\TestController::class, 'index']);

## 5) run http:://ip.no/test

`This is Title`


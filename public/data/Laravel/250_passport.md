# Passport
Laravel kullanıcı doğrulamak için `php artisan make:auth` ile standart doğrulama yapar, passport ise kullanıcı doğrulama yapmasının yanında sunucu sistemler arasında da doğrulama yapar.

> _Emrullah İLÇİN_

## 1 - install Passport
passport kurulur sql e ve storage ye oauth-private.key  oauth-public.key keyleri yazılır.


```
composer require laravel/passport
php artisan migrate
php artisan passport:install
```
## 2 - edit /app/User.php
```
use HasApiTokens,Notifiable
```

## 3 - edit /config/auth.php

```php
'api' => [
    'driver' => 'passport',
    'provider' => 'users',
],
```

## 4 - edit /app/Providers/AuthServiceProvider.php

```php
use Laravel\Passport\Passport;

public function boot()
{
    $this->registerPolicies();
    Passport::routes();
}
```

* `php artisan route:list`

| Domain | Method   | URI                                     | Name                              | Action                                                                    | Middleware   |
|--------|----------|-----------------------------------------|-----------------------------------|---------------------------------------------------------------------------|--------------|
|        | GET,HEAD | /                                       |                                   | Closure                                                                   | web          |
|        | GET,HEAD | api/user                                |                                   | Closure                                                                   | api,auth:api |
|        | POST     | oauth/authorize                         | passport.authorizations.approve   | Laravel\Passport\Http\Controllers\ApproveAuthorizationController@approve  | web,auth     |
|        | GET,HEAD | oauth/authorize                         | passport.authorizations.authorize | Laravel\Passport\Http\Controllers\AuthorizationController@authorize       | web,auth     |
|        | DELETE   | oauth/authorize                         | passport.authorizations.deny      | Laravel\Passport\Http\Controllers\DenyAuthorizationController@deny        | web,auth     |
|        | GET,HEAD | oauth/clients                           | passport.clients.index            | Laravel\Passport\Http\Controllers\ClientController@forUser                | web,auth     |
|        | POST     | oauth/clients                           | passport.clients.store            | Laravel\Passport\Http\Controllers\ClientController@store                  | web,auth     |
|        | PUT      | oauth/clients/{client_id}               | passport.clients.update           | Laravel\Passport\Http\Controllers\ClientController@update                 | web,auth     |
|        | DELETE   | oauth/clients/{client_id}               | passport.clients.destroy          | Laravel\Passport\Http\Controllers\ClientController@destroy                | web,auth     |
|        | GET,HEAD | oauth/personal-access-tokens            | passport.personal.tokens.index    | Laravel\Passport\Http\Controllers\PersonalAccessTokenController@forUser   | web,auth     |
|        | POST     | oauth/personal-access-tokens            | passport.personal.tokens.store    | Laravel\Passport\Http\Controllers\PersonalAccessTokenController@store     | web,auth     |
|        | DELETE   | oauth/personal-access-tokens/{token_id} | passport.personal.tokens.destroy  | Laravel\Passport\Http\Controllers\PersonalAccessTokenController@destroy   | web,auth     |
|        | GET,HEAD | oauth/scopes                            | passport.scopes.index             | Laravel\Passport\Http\Controllers\ScopeController@all                     | web,auth     |
|        | POST     | oauth/token                             | passport.token                    | Laravel\Passport\Http\Controllers\AccessTokenController@issueToken        | throttle     |
|        | POST     | oauth/token/refresh                     | passport.token.refresh            | Laravel\Passport\Http\Controllers\TransientTokenController@refresh        | web,auth     |
|        | GET,HEAD | oauth/tokens                            | passport.tokens.index             | Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@forUser | web,auth     |
|        | DELETE   | oauth/tokens/{token_id}                 | passport.tokens.destroy           | Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@destroy | web,auth     |


## 5 - edit /resources/assets/js/app.js
install sırasında passport için gerekli diğer componentler burada yazılımış halde.

```js
Vue.component('login', require('./components/Login.vue'));
```

## 6 - routes/api.php  

```php
Route::group(['middleware' => 'auth:api'], function () {
    Route::resource('crud', 'CrudController');
});
```

##  7 - "Login.vue" component 

```html
<template>
  <div class="container"><br>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Login</div>
                <div class="card-body">
                  <div class="alert alert-danger" v-if="error">
                      <p>There was an error</p>
                  </div>
                  <div class="form-group">
                      <label for="email">E-mail</label>
                      <input type="email" id="email" class="form-control"  v-model="email" required>
                  </div>
                  <div class="form-group">
                      <label for="password">Password</label>
                      <input type="password" id="password" class="form-control" v-model="password" required>
                  </div>
                  <button @click="login" class="btn btn-default">Sign in</button>
              </div>
            </div>
        </div>
    </div>
</div>
</template>
<script>
export default {
  data(){
    return {
      'error': null,
      'email': '',
      'password': ''
    }
  },
  methods: {
    login () {
      var  data = {
        client_id:2,
        client_secret:'XXXXXXXXXXXXXXXXXXXX',   /* sql de oauth_client' teki ikinci id secret'ini yaz */
        grant_type: 'password',
        username: this.email,
        password: this.password
      }
      axios.post('/oauth/token',data)
        .then(response => {
          console.log( 'Access token ::'+ JSON.stringify(response.data.access_token) ) ;   
          console.log( 'Expires ::'+ JSON.stringify(response.data.expires_in  + Date.now() ) ) ;  
        })
        .catch(error => {
            this.errors = [];   
            console.log('Post Error !! ');
        });  
    }
  }
}  
</script>

<style> 
</style>
```


## 8 - compile assets
```
npm install
npm run dev
```

## 9 - web.php

Route::get('/vue', function () {    return view('vue'); });

## 10 - vue.blade.php

```html
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <!-- Meta -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<!-- Styles -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  <div id="app">
       <login></login>
   </div>
  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" type="text/javascript"   ></script>	
</body>
</html>
```

## 11 - test 
Tarayıcının geliştirme araçları `console` undan bak.

`http://proje1.ilcin.name.tr/vue`


## First page 

> _Emrullah İLÇİN_

_Go to `cd $HOME/proje1` directory_

_For Routing;_<br>
* `echo "Route::get('/first_page', function () { return view('first_page'); });" >> ./routes/web.php`# write with Linux echo command or add `Route::get('/first_page', function () { return view('first_page'); });` in `./routes/web.php` file any with a editor
* `php artisan route:list` #  route test; see

_For View; use html tag, css or js;_<br>
* `./resources/views/first_page.blade.php` create file and write `first_page` or etc in; The extension should be ".blade.php"
* `echo "<b> $USER ' First Page </b>"  > ./resources/views/first_page.blade.php` # write with Linux echo command or add `"<b> $USER ' First Page </b>"` in `./resources/views/first_page.blade.php` file any with a editor
 
_Run web server port 8000 and host any_  (press `<ctrl>Z` to exit on Linux console)<br>
* `php artisan serve --host=localhost --port=8000`

_Browser_<br>
* `http://proje1.ilcin.name.tr/first_page` or `http://localhost/first_page`# and see; with browser 


# VIEW

Controller'den gelen ve/veya kendi üstünde oluşturulan verileri, Routing den aldığı url yoluyla, html,js ve css ile gösterir. 

_Emrullah İLÇİN_

## View dosyaları
* `$HOME/proje1/resources/views/`  # view lerin bulunduğu dizin

## Örnek ilksayfa.blade.php

```html
<!doctype html>
<html lang="tr">
 <head>
    <meta charset="utf-8">
    <title>Laravel</title>
    <script src="/js/app.js"></script>
    <link rel="stylesheet" href="/css/app.css">
  </head>
  <body>
    <b> İlksayfa </b>
  </body>
</html>
```

## layouts 
Her View sayfasında yazılan <head>, <body>, <title> gibi başlıkların bir şablon ile tanımlandıktan sonra sadece yollarını göstererek kullanılması. Default Layouts sayfası `$HOME/proje1/resources/views/layouts/app.blade.php` dir, app.blade.php de değişiklik yapılabilir veya yeni layouts dosyaları oluşturulabilir, dizin ve dosya farklı isimlerde de olabilir.

## Örnek layouts 

```html
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" </script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" </script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" </script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

</head>
<body>
<div id="app">
	<nav class="navbar navbar-light bg-light justify-content-between">
		<a class="navbar-brand">Navbar</a>
		<form class="form-inline">
			<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
			<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
		</form>
	</nav>

    @yield('content')
		
</div>

</body>
</html>

```

## layouts Kullanımı
Sayfamıza üstteki gibi bir layout örnek sürekli yazılacaksa ve sadece body içindeki  `@yield('content')` ile ifade edilen yerde değişiklik yapılacaksa yaratacağımız yeni web sayfaları şöyle olacaktır.

```html
@extends('layouts.app')
@section('content')

<!-- yeni sayfa için ilave olark yazılacaklar buyara yazabiliriz. -->

@endsection
```


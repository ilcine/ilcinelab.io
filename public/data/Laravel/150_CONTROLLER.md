# CONTROLLER

Controller, model den aldığı verileri kontrol eder, düzenler ve View e gönderir.

_Emrullah İLÇİN_

## Controller'i yaratma

* `php artisan make:controller IlkController` # Controller yaratılır.
* `php artisan make:controller IlkController --resource  --model=Task` # örnekte CRUD(yaratma,okuma,güncelleme,silme) için gerekli funksiyonları model ile birlikte şablon olarak yaratır.

> `$HOME/proje1/app/Http/Controllers/IlkController.php`  # Controller'in yazıldığı dosya

## laravel 8 notes

1) old way: write `RouteServiceProvider.php` in `protected $namespace = 'App\Http\Controllers';`

2) ex1: Route::get('/users', 'App\Http\Controllers\UserController@index');

3) ex2: web.php

```
use App\Http\Controllers\XController;
Route::get('/xx' , [XController::class,'index']);
```

4) ex3: api.php or web.php

```
Route::group(['namespace' => 'App\Http\Controllers\Api'], function () {
    Route::resource('user', 'UserController');
    Route::resource('book', 'BookController');
});
```
5) ex4:

```
Route::group(['namespace'=>'App\Http\Controllers', 'prefix'=>'admin',
 'as'=>'admin.','middleware'=>['auth:sanctum', 'verified']], function()
{
    Route::resource('/dashboard', 'DashboardController')->only([
        'index'
    ]);
});
```

6) ex:5 web.php
```
use App\Http\Controllers;
Route::get('book1', [Controllers\BookController::class, 'index']);
```



## Controller boş örneği (Resource ve modelli)

```php
<?php
namespace App\Http\Controllers;
use App\Task;  /* model adı  */
use Illuminate\Http\Request;
class IlkController extends Controller
{
    
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Task $task)
    {
        //
    }
    public function edit(Task $task)
    {
        //
    }
    public function update(Request $request, Task $task)
    {
        //
    }
    public function destroy(Task $task)
    {
        //
    }
}

```

:bulb: Controller içine yazılacak `//` veya `/* --- */` yorumdur derlemeye girmez.



<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class LogoutController extends Controller
{
    public function logout(Request $request)
    {
        /**
         * @var User $user
         */
        $user = $request->user();

        $user->currentAccessToken()->delete();

        event(new \Illuminate\Auth\Events\Logout('sanctum', $user));

        //return response()->noContent();
				return "silindi";
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
			
		
        $this->validate($request, [
            'email' => ['required', 'email:strict', 'max:255'],
            'password' => ['required', 'string'],
            'device_name' => ['required', 'string', 'max:255'],
        ]);

        if (! Auth::guard('web')->attempt($request->only(['email', 'password']))) {
            return response()->json([
                'message' => 'The provided credentials are incorrect.',
            ], Response::HTTP_BAD_REQUEST);
        }
//return 1;
        /**
         * @var User $user
         */
        $user = Auth::guard('web')->user();

        $token = $user->createToken(
            $request->input('device_name'),
            [
                'api',
            ]
        )->plainTextToken;


        return response()->json([
            'token_type' => 'Bearer',
            'access_token' => $token,
        ]);
    }
}

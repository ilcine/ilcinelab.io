# Validate
Sunucuya istemci tarafondan gönderilen verilerin kontrol edildiği yapı.
> _Emrullah İLÇİN_

## Example

```php
$this->validate($request, ['takipFoyu' => 'required']); 
$this->validate($request, ['hataTopla' => 'required']); 
```


```php
dizin kontrol
$this->validate($request, ['testTopla.*.testId' => 'required']); //ok.
```  


```php  
$this->validate($request, [
    'title' => 'required|min:3',
    'body' => 'required|min:10'
]);
```


```php
'create' => 'required|array',
  'create.*.artnr' => 'required|max:20',
  'create.*.unit' => 'max:20',
  'update' => 'required|array',
  'update.*.id' => 'required|exists:products,id',
  'update.*.artnr' => 'required|max:20',
  'update.*.unit' => 'max:20'
```

  
laravel e gelen tek boyutlu veri.  

```php    
$data=$request->only('takipFoyu');
    foreach($data as $k => $v){
			$c[] = ['vardiyaSuresi'   => $v['vardiyaSuresi']]; // ok
    }	
```

laravele gelen iki boyutlu veri.

```php
$data=$request->only('hataTopla');
	foreach($data as $k => $v){
			foreach($v as $k => $v){
				$c[] = ['hataTipi'   => $v['hataTipi']]; // ok
			}
	}
```  

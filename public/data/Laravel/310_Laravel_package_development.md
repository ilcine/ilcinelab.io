# Laravel package development.

Laravel'de paket geliştirme

_Emrullah İLÇİN_

## Requirement

Ubuntu 16.04 or 18.04, 

public ip address, 

gitlab.com and packagist.org account  

bash prompt, 

project name; ex: "example1",

Group name; ex: Emr,

### 1. Go to Project

1.1) bash // this command goes to bash shell

1.2) Create new laravel project 

`composer create-project --prefer-dist laravel/laravel proje1`

1.3) get into it if the laravel project was created

`cd proje1`

### 2. Create new package // it is create composer.json

2.1) package_name="example1"  //

2.2) group_name="emr"  //

2.3. `mkdir packages packages/$group_name packages/$group_name/$package_name packages/$group_name/$package_name/src` # Create directories

2.4) cd packages/$group_name/$package_name

2.5. or like this 

`mkdir packages packages/emr packages/emr/example1 packages/emr/example1/src` # Create directories

`cd packages/emr/example1`   # Go to project directory

2.6. `composer init`   # This process is created in the "composer.json" file

```
Package name (<vendor>/<name>) [emr/example1]:
Description []: Example1
Author [, n to skip]: Emrullah ILCIN <xyz@abc.com.>
Minimum Stability []: dev
Package Type (e.g. library, project, metapackage, composer-plugin) []: project
License []: MIT
Would you like to define your dependencies (require) interactively [yes]? no
Would you like to define your dev dependencies (require-dev) interactively [yes]? no
Do you confirm generation [yes]? y
```

2.4. `composer config prefer-stable true`  # This command writes  __"prefer-stable": true,__  to the "composer.json"

and 
`composer config minimum-stability dev`

2.5. `composer config extra.laravel.providers "Emr\\Example1\\Example1ServiceProvider"` # This commands writes 
__"extra": { "laravel": { "providers": "Emr\\Example1\\TodoServiceProvider" } }__ to the "composer.json"

2.6. `jq '.autoload."psr-4" += {"Emr\\Example1\\": "src/"}' composer.json > composer.1` # this commands writes 
__"autoload": { "psr-4": { "Emr\\Example1\\": "src/" }}__ to the "composer.1" 

> If jq is not installed, install jq `apt-get install jq -y`

>  if you will install other packages; Ex: passport `jq '.require += {"laravel/passport": "^7.0"}' composer.json > composer.2`

2.7. `cp composer.2 composer.json; rm composer.2`  # copy composer.2 to composer.json and delete composer.1

~~composer dump-autoload~~  # ~~vendor create.~~

2.8. if you want to see "composer.json file" use `more composer.json`

> latest status of "composer.json" file

```
{
  "name": "emr/example1",
  "description": "Example",
  "type": "project",
  "license": "MIT",
  "authors": [
      {
          "name": "Emrullah ILCIN",
          "email": "emr@uludag.edu.tr"
      }
  ],
 
    "require": {  },
    "autoload": {
      "psr-4": {
          "Emr\\Example1\\": "src/"
      }
    },
     "extra": {
      "laravel": {
        "providers": [
          "Emr\\Example1\\Example1ServiceProvider"
        ]
      }
    },
	 "minimum-stability": "dev",
	 "prefer-stable": true
}
```

2.9. Create "README.md" file if you want. `echo -e "# Packet name example1 and etc.. " > README.md`


### 3. Create end edit Provider

> Use skeleton directory of laravel  (laravel in iskelet dizinini kullan.) this bash command automatically creates standard directories

3.1 This command create "src src/Providers src/Http src/Http/Middleware src/Http/Controllers src/Http/Controllers/Auth src/Console src/Exceptions" 
directorys
```bash
find $HOME/proje1/app -type d -path "*/app*" | xargs | mkdir -p $(sed s-$HOME/proje1/app-src-g)
```

3.2. This command create "bootstrap bootstrap/cache routes config resources resources/js resources/js/components resources/resources resources/resources/views resources/sass resources/views resources/views/layouts resources/views/vuecrud resources/views/auth resources/views/auth/passwords resources/lang resources/lang/en public public/js public/css public/svg public/vuecrud public/vuecrud/components tests tests/Feature tests/Unit database database/migrations database/factories database/seeds"
directorys

```bash
find $HOME/proje1 -type d ! -path "*/app*" \
! -path "*/vendor*" ! -path "*/packages*" ! -path "*/node_modules*" ! -path "*/storage*"  | \
xargs | mkdir -p $(sed s-$HOME/proje1/--g)
```

3.3. This commands creates other files and put "<?php " into it. 
```bash
echo -e "<?php" > src/Providers/Example1ServiceProvider.php
echo -e "<?php" > routes/web.php
echo -e "<?php" > src/Http/Controllers/Example1Controller.php
echo -e "Test 1" > resources/views/test1.blade.php
echo -e "Test 2" > resources/views/test2.blade.php
echo -e "epoch time: {{ \$tasks }}" > resources/views/test3.blade.php
```

3.4. edit `src/Providers/Example1ServiceProvider.php` 
 
```php
<?php
namespace Emr\Example1;  // =src dizini; laraveldeki app 

use Illuminate\Support\ServiceProvider;
use Emr\Example1\Http\Controllers\Example1Controller;

class Example1ServiceProvider extends ServiceProvider
{

    public function boot()
    {
       // see:  ..  https://laravel.com/docs/5.7/packages
       $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
       $this->loadViewsFrom(__DIR__. '/../../resources/views', 'courier'); // Use `courier::` in the Controllers section
    }
    
    public function register()
    {
        
    }
    
}
```
> Other forms:`$this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');` and etc...

### 4. Creade and edit routes/web.php

edit `routes/web.php`

```
<?php

Route::group([
  'namespace' => "Emr\Example1\Http\Controllers",
  'prefix' => 'audit',
        ],  function () {
  Route::get('/test1', function () {  echo "test1 src - audit altındaki"; }); // http://94.177.187.77:8000/audit/test1
  Route::get('/test2','Example1Controller@test2'); // http://94.177.187.77:8000/audit/test2
  Route::get('/test3','Example1Controller@test3'); // http://94.177.187.77:8000/audit/test3
});

```

### 4.1 Controller

edit `src/Http/Controllers/Example1Controller.php`
```
<?php
namespace Emr\Example1\Http\Controllers;
use App\Http\Controllers\Controller;

class Example1Controller extends Controller
{

  public function test2()
  {
     echo "test2 e geldi";
  }
  public function test3()
  {
    $tasks = time(); 
    return view('courier::test3', ['tasks' => $tasks]);  // http://94.177.187.77:8000/audit/test2
  }
  
  public function test5()
  {
     echo "test5 e geldi";
  }
 
}

```

> Ex: if you want to use admin subdirectory; use `courier::admin.test2`

### 4.2 View

`echo -e "Test 2"  > resources/views/test2.blade.php`

`echo -e "epoch time: {{ $tasks }}"  > resources/views/test3.blade.php`

### 5. Main composer.json update and run with LOCAL repositories

5.1. cd $HOME/proje1

5.2.  `composer config repositories.local '{"type": "path", "url": "./packages/emr/example1"}' --file composer.json`

> if packagit is active `composer config repositories.packagist false` and `composer remove emr/example1` and `composer clearcache` and `composer dump-autoload`

5.3. `composer require emr/example1`  #   `more composer.json` See repositories and reguire in composer.json 

> if no memory; 

<pre>
df -h 
dd if=/dev/zero of=/swapfile bs=1M count=1024
sudo dd if=/dev/zero of=/swapfile bs=1M count=1024
mkswap /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
echo 'echo "/swapfile  none  swap  defaults  0  0" >> /etc/fstab' | sudo sh

free -m
</pre>

> if it still says no memor //  use;  composer update


5.4. `php artisan serve --host=94.177.187.77 --port=8000`

5.5. Test page:  `http://94.177.187.77:8000/audit/test2`


> If you don't have "repositories.local", "vendor" is active.

> if you want to delete the LOCAL repository ; RUN  `composer config --unset repositories.local` and `composer remove emr/example1` 
and `composer config --unset repositories.packagist` and `composer clearcache` and `composer dump-autoload`

> ex: gitlab repo: `composer config repositories.gitlab-10 '{"type": "vcs", "url": "https://gitlab.com/ilcine/example1"}' --file composer.json`


### 7. Send your project to Gitlab

> Register if you have not registered; Get username and password.

Install new project on Gitlab

```
cd $HOME/proje1/packages/emr/example1
git init
git add .
git commit -m "first commit"

git remote add gitlab https://gitlab.com/ilcine/example1.git  
git push -u gitlab --all # depoya yazar
git tag -a 1.0.0 -m "release: First version"
git push -u gitlab --tags 
```

> `git pull` retrieve changes from remote host


Go to gitlab web page (http://gitlab.com); Select "Public"

`/Projects /Your projects/example1/Settings/General/Permissions/[Expand]/Project visibility:Public`

If you make changes to the project page.

```
git add .
git commit -m 'New Update'
git push
git tag -a 1.0.1 -m "release: second version"
git push -u gitlab --tags 
```

### 8. Send your project to Packagist 

> Go to Packagist web page (https://packagist.org);

> Register if you have not registered; Get username and password.

Chose "Submit" // "Repository URL (Git/Svn/Hg)" and enter gitlab project link; ex:`https://gitlab.com/ilcine/example1.git`

See "My packages" links; You will see your own package.

> If you need api:  Chose `/Profile/Show API Token`; see token; get token; 
> To enable the GitLab service integration, go to your GitLab repository, 
   open the Settings > Integrations page from the menu. 
   Search for Packagist in the list of Project Services. 
   Check the "Active" box, enter your packagist.org username and API token. 
   Save your changes and you're done.


> for public; Go to `Gitlab/Setting/General/Visibility/expand/Project/Visibility/Public` enable

### 9. Use the "emr.example1" package in a new project with PACKAGIST.ORG

> if composer.json "repositories.local" is active and an existing project; RUN `cd projectX` and
 `composer config --unset repositories.local` and `composer remove emr/example1` and `composer config --unset repositories.packagist`
and `composer clearcache` and `composer dump-autoload`

> If new project will be made `cd $HOME` and  `composer create-project --prefer-dist laravel/laravel proje2` and  `cd proje2`

```
composer require emr/example1
php artisan serve --host=94.177.187.77 --port=8000
```

Test page. `http://94.177.187.77:8000/audit/test2`

> If the emr/example1 project in Gitlab has changed; use `composer remove emr/example1` and `composer require emr/example1:1.0.1`



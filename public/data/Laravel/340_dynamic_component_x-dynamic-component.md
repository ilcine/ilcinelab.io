# Dynamic_component with "x-dynamic-component"

## 1) routes/web.php

use Illuminate\View\ComponentAttributeBag; // for "dynamic component Attribute"

```
Route::get('/dynamic/{id}', function ($id) {
	return view('welcome1', [
			'component' => $id,  //  ori: 'foo'
			'value' => '<div>abc</div>',
			'myAttributes' => new ComponentAttributeBag([
					'style' => 'font-style: italic;',
			]),
	]);
});
```

## 2) resources/views/welcome.blade.php


```
<html>
<head>
<body>		
  <x-dynamic-component :component="$component" :attributes="$myAttributes" :value="$value" />

	<p {{ $myAttributes }}> This attribute italic </p>

	<p> {{ $value }} </p>
	
	<p id="demo" {{ $myAttributes }}  ></p>

</body>
</head>

<script>
var x = "<?php echo $value ?>"
document.getElementById("demo").innerHTML = x ;  
</script>
```

## 3) foo components in;   `resources/views/components/foo.blade.php`

```
@props([
    'value',
])

<div>
    @dump($value)
</div>
```

## 4) run http://ip.no/dynamic/foo

4.1) result

`"<div>abc</div>"`<br>
_This attribute italic_<br>
`<div>abc</div>`<br>
<i> abc</i><br>
	
4.2) in view source

```
<html>
<head>
<body>
  <div>
	
    <script>
       bla bla ... abc 
    </script>
		
		<p style="font-style: italic;"> This attribute italic </p>
		<p> &lt;div&gt;abc&lt;/div&gt; </p>
		<p style="font-style: italic;"> abc </p>

	</div>  
</body>
</head>
```
	



# Sancturm

- Laravel Sanctum provides a featherweight authentication system for SPAs (single page applications), 
mobile applications, and simple, token based APIs.

- Sanctum is a simple package you may use to issue API tokens 
to your users without the complication of OAuth. 

- Sanctum uses Laravel's built-in cookie based session authentication services.
 
- Use Sanctum only for API token authentication or only for SPA authentication.


## Go to laravel project or create 

1) composer create-project --prefer-dist laravel/laravel proje1

2) composer require laravel/sanctum

3) php artisan vendor:publish --provider="Laravel\Sanctum\SanctumServiceProvider"

4) php artisan make:model Book -mr

5) edit database/migrations/???_create_books_table.php

```
 $table->string('title');
 $table->string('author');
```		

6) php artisan migrate

7) php artisan make:seeder BookSeeder

7.1) edit `database/seeders/BookSeeder.php`

```
use App\Models\Book;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

public function run()
  {	
		$faker = \Faker\Factory::create();
		
		for ($i = 0; $i < 10; $i++) {
				Book::create([
						'title' => $faker->sentence,
						'author' => $faker->name,
				]);
		}
	
		DB::table('users')->insert([
			'name'=>'emr',
			'email'=>'emr@emr.com',
			'password'=>Hash::make('123456')
			]);			
				
  }
```

7.2) php artisan db:seed --class=BookSeeder
 
8) edit routes/api.php

```
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['guest:sanctum'], 'as' => 'auth.'], function () {
		Route::post('/login', [App\Http\Controllers\Auth\LoginController::class, 'login']);
		Route::post('/register', [App\Http\Controllers\Auth\RegisterController::class, 'register']);
});

Route::group(['middleware' => ['auth:sanctum'],], function () {		
		Route::post('/logout', [App\Http\Controllers\Auth\LogoutController::class, 'logout']);
});

Route::post('/me', [App\Http\Controllers\MeController::class, 'me'])->middleware('auth:sanctum');
Route::get('book', [App\Http\Controllers\BookController::class, 'index'])->middleware('auth:sanctum');

```

9) app/Http/Controllers/BookController.php

add `return response()->json(Book::all());` in index function

10) book db test

php artisan serve

curl http://localhost:8000/api/book

11) app/Http/Kernel.php

- aAdd Sanctum middleware to your api middleware group within your application's

'api' => [
    \Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful::class,
    'throttle:api',
    \Illuminate\Routing\Middleware\SubstituteBindings::class,
],

12) edit /app/models/user.php

```
use Laravel\Sanctum\HasApiTokens;
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
}
```

13) edit config/sanctum.php

```
'stateful' => explode(',', env(
        'SANCTUM_STATEFUL_DOMAINS',
        'localhost,localhost:3000,127.0.0.1,127.0.0.1:8000,::1'
    )),
```		

14) edit .env

```
SANCTUM_STATEFUL_DOMAINS=localhost
SESSION_DRIVER=cookie
```

15) edit config/cors.php

```
'paths' => [
    'api/*',
    '/login',
    '/logout',
    '/sanctum/csrf-cookie'
],

'supports_credentials' => true

```

16) controllers

16.1) edit App/Http/Controllers/MeController.php

```
<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class MeController extends Controller
{
		public function me(Request $request)
		{
			return  $request->user();
		}
}	
```

16.2) edit App/Http/Controllers/Auth/LoginController.php

```
<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => ['required', 'email:strict', 'max:255'],
            'password' => ['required', 'string'],
            'device_name' => ['required', 'string', 'max:255'],
        ]);

        if (! Auth::guard('web')->attempt($request->only(['email', 'password']))) {
            return response()->json([
                'message' => 'The provided credentials are incorrect.',
            ], Response::HTTP_BAD_REQUEST);
        }

        $user = Auth::guard('web')->user();

        $token = $user->createToken(
            $request->input('device_name'),
            [
                'api',
            ]
        )->plainTextToken;


        return response()->json([
            'token_type' => 'Bearer',
            'access_token' => $token,
        ]);
    }
}
```

16.2) edit App/Http/Controllers/Auth/LogoutController.php

```
<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class LogoutController extends Controller
{
    public function logout(Request $request)
    {

        $user = $request->user();

        $user->currentAccessToken()->delete();

        event(new \Illuminate\Auth\Events\Logout('sanctum', $user));

        //return response()->noContent();
				return "silindi";
    }
}
```

16.3) edit App/Http/Controllers/Auth/RegisterController.php

```
<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'min:3', 'max:255',],
            'email' => ['bail', 'required', 'email:strict', 'max:255', 'unique:users,email'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        $user = new User();
        $user->fill([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
        ]);
        $user->save();

        event(new \Illuminate\Auth\Events\Registered($user));

        return response()->noContent();
    }
}

```

17) test with curl 

17.1) register  // write to "users" table

```
curl --request POST \
    --url http://localhost:8000/api/register \
    --header 'Content-type: application/json' 	\
    --data '{"name": "emr", "email": "emr@uludag.edu.tr", "password": "12345678", "password_confirmation": "12345678"}' 
```		
		
17.2) login // write to `personal_access_tokens` 

```
curl --request POST \
    --url http://localhost:8000/api/login \
    --header 'Content-type: application/json' 	\
    --data '{"email": "emr@uludag.edu.tr", "password": "12345678", "device_name": "api"}'  
```		
		 
return; ex 		`{"token_type":"Bearer","access_token":"1|K4fQysLV6xUE0HeJGux1ZUo0SX3TXxF03onkWD0"}` 
		 
17.3) login test // 

```
curl	--request POST \
  --url http://localhost:8000/api/me \
  --header 'Authorization:Bearer 1|K4fQysLV6xUE0HeJGux1ZUo0SX3TXxF03onkWD0' \
  --header 'Content-type: application/json' 
```

Response; read user record 

ex:	`{"id":1,"name":"test","email":"emr@uludag.edu.tr","email_verified_at":null,"created_at":"2021-04-04T16:58"}
	
17.4) logout

```
curl --request POST \
  --url http://localhost:8000/api/logout \
  --header 'Authorization:Bearer 1|K4fQysLV6xUE0HeJGux1ZUo0SX3TXxF03onkWD0' \
  --header 'Content-type: application/json' 
```

17) other api tools	   

	17.1) curl (we will use this)
	17.2) postman for windows
	17.3) insomnia for windows
  17.4) Talend API Tester Chrome Extension	

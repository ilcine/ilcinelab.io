/*		
		https://stackoverflow.com/questions/8141125/regex-for-password-php
		http://www.aydinmahmut.com/regex-kullanimi-php/
		^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$
		Zorched'daki iyi insanlardan .

		^: dizginin başlangıcına bağlandı
		\S*: herhangi bir karakter kümesi
		(?=\S{8,}): en az 8
		(?=\S*[a-z]): en az bir küçük harf içeren
		(?=\S*[A-Z]): ve en az bir büyük harf
		(?=\S*[\d]): ve en az bir numara
		$: dizenin sonuna bağlandı
		Özel karakterler eklemek için, sadece (?=\S*[\W])kelime olmayan karakterler olan ekleyiniz .
		
		EX2:
		if(preg_match((?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$), $_POST['password']):

		
		*/
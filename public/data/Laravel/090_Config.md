# Laravel Config 

In the Laravel configuration, settings are made to the `.env` file or `./config/*` directory. 
Files in `./config/*` directory (app.php, auth.php, broadcasting.php, cache.php, database.php, filesystems.php, hashing.php, logging.php, mail.php, queue.php, services.php, session.php, view.php and etc). [Sample ".env" file:](./src/090_Config_in_env.md)

> _Emrullah İLÇİN_

## Set the project name

_In `.env` file_  
`APP_NAME="Proje Name"`

_Or in `./config/app.php` file_  
`'name' => env('APP_NAME', 'Project name'),`

## To set the time zone 

_In `config/app.php` file_ <br>
`'timezone' => env('APP_TIMEZONE', 'Europe/Istanbul'),` // For Turkey/Istanbul<br>
`'locale' => 'tr', # local setting,` for Turkish

## Debug setting

Debug (error checking and tracking) settings on the file are set "true" or "false". The default ".env" is also "true". Do "false" when the project is completed

_In `./config/app.php` file_<br>
`'debug' => env('APP_DEBUG', true),` // true or false <br>

_Or in `.env` file_<br>
`APP_DEBUG=true` or `APP_DEBUG=false`

* _Debug blacklist_ 

Attention: After misidentification, all sensitive environment variables (db passwords, mail passwords, application keys, etc.) will be seen. All of these should be closed when the application exits debug mode.

Add the following at the end of the `config/app.php` file to turn off only important passwords and api keys

```php
'debug_blacklist' => [
        '_ENV' => [
            'APP_KEY',
            'DB_PASSWORD',
            'REDIS_PASSWORD',
            'MAIL_PASSWORD',
            'PUSHER_APP_KEY',
            'PUSHER_APP_SECRET',
        ],
        '_SERVER' => [
            'APP_KEY',
            'DB_PASSWORD',
            'REDIS_PASSWORD',
            'MAIL_PASSWORD',
            'PUSHER_APP_KEY',
            'PUSHER_APP_SECRET',
        ],
        '_POST' => [
            'password',
        ],
    ],

```

Closing all `.env` variables 

```php
'debug_blacklist' => [
        '_COOKIE' => array_keys($_COOKIE),
        '_SERVER' => array_keys($_SERVER),
        '_ENV' => array_keys($_ENV),        
    ],
```

> If an incorrect return syntax is used in php, all ".env" variables will be seen as "* * *". This ensures your safety

## Logging

_In `./config/logging.php` file_<br>
`'default' => env('LOG_CHANNEL', 'single'),`  // single log <br>
`'default' => env('LOG_CHANNEL', 'stack'),`  // default stack log with date; ex: laravel-2019-06-07.log

_Or in `.env` file_<br>
`LOG_CHANNEL=stack` or  `LOG_CHANNEL=single` or etc.

## Session 

`file` sessions are stored in the file `.storage/framework/sessions`  (default)<br>
`cookie` sessions are stored in secure, encrypted cookies.<br>
`database` sessions are stored in a relational database.<br>
`memcached` the session is stored in the memcached cache ( memory)<br>
`redis` The session is stored in the redis cache ( memory)<br>
`array` sessions are stored in a PHP directory, not persistent<br>
and etc.

* _ex: To store sessions on DataBase_

Run this in the linux console in the project

```php
php artisan session:table
php artisan migrate
```

This is created in the db scheme.

```php
Schema::create('sessions', function ($table) {
    $table->string('id')->unique();
    $table->unsignedInteger('user_id')->nullable();
    $table->string('ip_address', 45)->nullable();
    $table->text('user_agent')->nullable();
    $table->text('payload');
    $table->integer('last_activity');
});
```
_ In `.env`  file_<br> 
`SESSION_DRIVER=database`  // change to database

## If MySql version is less than 5.4

Make this change in file `./app/Providers/AppServiceProvider.php`

```php
use Illuminate\Support\Facades\Schema;  // add line
public function boot()
    {
     Schema::defaultStringLength(191);  // if empty, add line
    }
```
## MySql strict setting

_In `.config/databese.php` file_<br>
`'strict' => true,` or `'strict' => false,`

> [look for the detailed "mysql strict": ](https://gitlab.com/ilcine/UbuntuNotes/blob/master/src/840_mysql.md)


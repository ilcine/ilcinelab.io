# Homestead install for Windows

> _Emrullah İLÇİN_

Laravel projeleri için hazır Homestead kurulumu

## First process;  Download and install this package

1) virtualbox 6 ; sanal vm

https://www.virtualbox.org/wiki/Downloads

2) vagrant; pc ile virtualbox arasındaki ilişki
https://www.vagrantup.com/downloads.html

3) git bash ; console ; shell command on windows
https://gitforwindows.org/

4) go to console "git bash"   // ex:  `pwd`  `/c/User/emr`; bash prompt is "$"

4.1) $ `vagrant box add laravel/homestead`

```
1) hyperv
2) parallels
3) virtualbox
4) vmware_desktop

Enter your choice: 
```
your choice: virtualbox is number `3`

4.3) $ `git clone https://github.com/laravel/homestead.git ~/Homestead`

4.4) $ `cd ~/Homestead`

4.5) `./init.bat`


4.5) Windows notepad(admin mode) `C:\Windows\System32\drivers\etc\hosts` and add `192.168.10.10 homestead.app`


4.6) edit Homestead.yaml 

```
folders:
    - map: 'C:\Users\emr\GIT\vagrant'
      to: /home/vagrant/code

sites:
    - map: homestead.app
      to: /home/vagrant/code/public
```      

4.7) ssh 
 
`ssh-keygen -t rsa -b 4096 -C "emr@homestead.app"`   ;enter/enter/enter

`eval "$(ssh-agent -s)"`

`ssh-add -k ~/.ssh/id_rsa`
 
  _response;  Identity added: /c/Users/emr/.ssh/id_rsa (emr@homestead.app)_

4.8) run
  
`vagrant up`

`vagrant ssh`

4.9) cd code; cd public

`echo "test page" > index.html`

4.10) `vi /etc/nginx/sites-available/virtual.host.conf`

# create new

```
server {
    listen       80;
    server_name  homestead.app;

    location / {
        root   /home/vagrant/code/public;
        index  index.html index.htm;
    }
}
```

4.11) `systemctl restart nginx` 


4.12) Browser

`http://192.168.10.10/`

or `http://homestead.app`

ok.

4.13)  system halt

`vagrant halt`

4.14) create new virtual host

Windows PC

notepad(admin mode) `C:\Windows\System32\drivers\etc\hosts` and add `192.168.10.10 homestead.proje1`

Homestead.yaml

```
folders:
    - map: 'C:\Users\emr\GIT\vagrant'
      to: /home/vagrant/code
      
    - map: 'C:\Users\emr\GIT\vagrant\proje1'
      to: /home/vagrant/code/proje1 

sites:
    - map: homestead.test
      to: /home/vagrant/code/public
      
    - map: homestead.proje1
      to: /home/vagrant/code/public/proje1/public
```      

`vagrant up --provision`  ; created virtual host config on nginx;  `/etc/nginx/sites-available/homestead.proje1`

Browser

`http://homestead.proje1`




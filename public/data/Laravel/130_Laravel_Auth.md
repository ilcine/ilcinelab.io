# Auth
Laravel'in içindeki Kimlik doğrulamanın kullanımı. Laravel, Session ve Authentication yöntemleri kullanarak kullanıcıları doğrular.
> _Emrullah İLÇİN_

## auth install 
*  php artisan make:auth

```
./routes/web.php  //  yönlendirme yapılan sayfa
./app/Http/Controllers/HomeController.php  // controller sayfasi
./resources/views/layouts/app.blade.php  // nav ve footer,  ve html şablonu oluşur. 
./resources/views/home.blade.php // ilk sayfa
./resources/views/auth/passwords/reset.blade.php
./resources/views/auth/passwords/email.blade.php
./resources/views/auth/register.blade.php
./resources/views/auth/login.blade.php

config/auth.php // konfigurasyon dosyası
```

## Çalışma mantığı
web.php ile yönlendirmeler tanımlanır İlk yönlendirme “/” ile Route::get ile return view(‘welcome’) ile tanımlı fonksiyondur bu fonksiyon ilk sayfayı   HomeController e göndermeden “resources/view/welcome.blade.php” yolundaki dosya ile çağırır. (welcome dosya adı sonuna “blade.php” takısı konulur) ilk sayfa bu işlemle açılır.

auth ile kurulum yapıldığı için welcome sayfasında login ve register link leri çıkar. login seçildiğinde  “resources/view/auth/login.blade.php sayfası gelir. login sayfası formu post yöntemi ile  action="{{ route('login') }}"  eylemi ile web.php deki “Auth::routes()“ fonksiyonu çağırılır bu da “app/Http/Controller/auth/LoginController.php” a yönlendirir burada doğrulama yapılır ve  sayfa web.php deki “/home” yönlendirme ile app/Http/Controller/HomeController.php ye gönderilir, (index) fonksiyonu çalışır o da ilk sayfayı “return view(‘home’) ile (resources/view/home.blade.php)  a gösterir. (not: ilk home sayfası yetki kontrolu de   “_contruct” fonkisyonu ile yapar)

Yetkileme hatalı, şifre unutma ve hatırlatma gibi kontroller de app/Http/Controller/Auth altında bulunur.

## Routing 
`routes/web.php` dosyasına yapılır. default tanımıdır, proje ilk sayfası welcome.blade.php üzerinde yapılacak değişiklikle olabilir.

```php
Route::get('/', function () { return view('welcome'); });
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
```
 
`php artisan route:list`
 
| Domain | Method   | URI                    | Name             | Action                                                                 | Middleware   |
|--------|----------|------------------------|------------------|------------------------------------------------------------------------|--------------|
|        | GET,HEAD | /                      |                  | Closure                                                                | web          |
|        | GET,HEAD | api/user               |                  | Closure                                                                | api,auth:api |
|        | GET,HEAD | home                   | home             | App\Http\Controllers\HomeController@index                              | web,auth     |
|        | GET,HEAD | login                  | login            | App\Http\Controllers\Auth\LoginController@showLoginForm                | web,guest    |
|        | POST     | login                  |                  | App\Http\Controllers\Auth\LoginController@login                        | web,guest    |
|        | POST     | logout                 | logout           | App\Http\Controllers\Auth\LoginController@logout                       | web          |
|        | POST     | password/email         | password.email   | App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail  | web,guest    |
|        | GET,HEAD | password/reset         | password.request | App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm | web,guest    |
|        | POST     | password/reset         |                  | App\Http\Controllers\Auth\ResetPasswordController@reset                | web,guest    |
|        | GET,HEAD | password/reset/{token} | password.reset   | App\Http\Controllers\Auth\ResetPasswordController@showResetForm        | web,guest    |
|        | GET,HEAD | register               | register         | App\Http\Controllers\Auth\RegisterController@showRegistrationForm      | web,guest    |
|        | POST     | register               |                  | App\Http\Controllers\Auth\RegisterController@register                  | web,guest    |


## Controller
`app/Http/Controllers/HomeController.php` dosyasındadır.

```php
<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	//dd( session()->all() ); // Sessionlara bak
	//dd( session()->token() );  // Session un ürettiği token e bak
	//dd( auth()->check() ); // true veya false doner
	//dd( auth()->user()->name ); // Session ile auth olan kullanıcı adına bak.
      
        return view('home');
    }
}
```

## Session Log with "file"

Was created it "storage/framework/sessions/XXXXnew fileXXXXXX" file when login; and when logout it is deleate and create new log file

Example login log

```
a:4:{s:6:"_flash";a:2:{s:3:"old";a:0:{}s:3:"new";a:0:{}}
    s:6:"_token";s:40:"WVLPPr2jWUKBiM7o0FyFzWohE7KxEmSan8nzDDI5";
    s:9:"_previous";a:1:{s:3:"url";s:32:"http://proje1.ilcin.name.tr/home";}
    s:50:"login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d";i:1;}
```

> The generated csrf-token key is also used in html. `<meta name="csrf-token" content="WVLPPr2jWUKBiM7o0FyFzWohE7KxEmSan8nzDDI5">`

> if you look at `dd(session()->token());` in controls; you will see token `"WVLPPr2jWUKBiM7o0FyFzWohE7KxEmSan8nzDDI5"`


Example logout log

```
a:3:{s:6:"_flash";a:2:{s:3:"old";a:0:{}s:3:"new";a:0:{}}
    s:6:"_token";s:40:"fyoCgv9GQzezFfHUsB2aS2LUKXpbQSp1nmA4BSam";
    s:9:"_previous";a:1:{s:3:"url";s:27:"http://proje1.ilcin.name.tr";}}
```

> Although "php artisan make:auth" is not active, this log file is gives for csrf-token in new html file


## Test ve Kullanım
* http://proje1.ilcin.name.tr/register
* http://proje1.ilcin.name.tr/login

:bulb: logout: (nav menunun sonunda logout functionu vardir logout blade dosyasi değildir.)


## ex: admin auth (laravel 6)

```
1) php artisan ui vue --auth
2) php artisan make:middleware Admin
3) vi app/Http/middleware
--
if(auth()->user()->id == 1){
	 return $next($request);
		 }
	return redirect(‘home’)->with(‘error’,"You don't have admin access.");
--

4) app/Http/Kernel.php	(IN 'protected $routeMiddleware')

'admin' => \App\Http\Middleware\Admin::class,

5) use; controller or route
5.1) controller
--
public function __construct()
    {
        $this->middleware('admin');
    }
--
5.1) or route
Route::post('/login', 'Api\LoginController@login')->middleware('admin');

6) see route
php artisan route:list

```



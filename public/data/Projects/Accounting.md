# Accounting 

This General Accounting project has been used for 4 years in a company for transactions such as day-book, 
Trial balance, Ledger, Budget, Period On / Off etc. 
The code is written on Linux Slackware 10.2(kernel 2.4) with the programming language "Progress v9". 
New versions of Progress can be applied by modifying "Progress | OpenEdge".

Bu Genel Muhasebe projesi Yevmiye, Mizan, Defteri Kebir, Bütçe, Dönem Açma/Kapama
vb işlemleri için bir firmada 4 yıl boyunca kullanılmıştır. 
Kod "Progress v9" programlama dili ile Linux Slackware 10.2 (Kernel 2.4) üzerinde yazılmıştır. 
Progress in yeni sürümleri "Progress|OpenEdge" de değişiklik yapılarak uygulanabilir.


https://gitlab.com/ilcine/AccountingNotes/

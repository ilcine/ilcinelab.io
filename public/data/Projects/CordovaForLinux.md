# Cordova For Linux


## 1) Create packages 
```
sudo npm install -g @vue/cli
sudo npm i -g @vue/cli-init
sudo npm install -g cordova
cordova --version  // 10.0.0
node --version
```

## 2) for cordova requirements android studio and java install

### 2.1) Install android studio on ubuntu 20.04 with snap

```sudo snap install android-studio```

>> for andoid tools; go to  https://developer.android.com/studio

```
// directory : /home/emr/Android/Sdk/tools/bin/sdkmanager --sdk_root=${ANDROID_HOME} 
sdkmanager --sdk_root=${ANDROID_HOME} "platform-tools" "platforms;android-28"
sdkmanager --sdk_root=${ANDROID_HOME} "platforms;android-29"
sdkmanager --sdk_root=${ANDROID_HOME} "tools"
sdkmanager --sdk_root=${ANDROID_HOME} "build-tools;29.0.2"

sdkmanager --sdk_root=${ANDROID_HOME} --version  
sdkmanager --sdk_root=${ANDROID_HOME} --licenses
sdkmanager --sdk_root=${ANDROID_HOME} --list|more


for erase: ex:
sdkmanager --sdk_root=${ANDROID_HOME} --uninstall "build-tools;27.0.3"
```


### 2.2) install android with ppa packages 

https://vitux.com/how-to-install-android-studio-on-ubuntu-20-04/

```
sudo apt purge android-sdk\*
sudo apt-get autoremove
sudo add-apt-repository ppa:maarten-fonville/android-studio	
apt update
sudo apt install android-studio
sudo apt-get install ppa-purge // delete full ppa
``` 
	
> directory where it is installed "/usr/lib/android-sdk"
> use this directory "/home/$HOME/Android/Sdk/"

### 2.2) install android-sdk "do not install android-studio"

make PATH
```
mkdir $HOME/Android
mkdir $HOME/Android/Sdk
export ANDROID_HOME="/home/emr/Android/Sdk/"    
export ANDROID_SDK_ROOT="/home/emr/Android/Sdk/"
export PATH=${PATH}:ANDROID_SDK_ROOT/emulator:$ANDROID_SDK_ROOT/tools:$ANDROID_SDK_ROOT/platform-tools
```

>> and write in ".bashrc" | and run "source .bashrc"

download sdkmanager and etc.
```
download "sdkmanager" from ( https://developer.android.com/studio  inside commandlinetools-linux-7302050_latest.zip )

cp -rf cmdline-tools $HOME/Android/Sdk/.
```



install platforms, platform-tools, tools and etc.
```
// directory : /home/emr/Android/Sdk/cmdline-tools/bin/sdkmanager --sdk_root=${ANDROID_HOME} 
sdkmanager --sdk_root=${ANDROID_HOME} "platform-tools" "platforms;android-28"
sdkmanager --sdk_root=${ANDROID_HOME} "platforms;android-29"
sdkmanager --sdk_root=${ANDROID_HOME} "tools"
sdkmanager --sdk_root=${ANDROID_HOME} "build-tools;29.0.2"

sdkmanager --sdk_root=${ANDROID_HOME} --version  
sdkmanager --sdk_root=${ANDROID_HOME} --licenses
sdkmanager --sdk_root=${ANDROID_HOME} --list|more


for erase: ex:
sdkmanager --sdk_root=${ANDROID_HOME} --uninstall "build-tools;27.0.3"
```

>> cmdline-tools/bin/avdmanager list  // see

### 2.3) android path

```
export ANDROID_HOME="/home/emr/Android/Sdk/"	
export ANDROID_SDK_ROOT="/home/emr/Android/Sdk/"
export PATH=${PATH}:ANDROID_SDK_ROOT/emulator:$ANDROID_SDK_ROOT/tools:$ANDROID_SDK_ROOT/platform-tools

export ANDROID_AVD_HOME=/home/emr/.android/avd
export PATH=${PATH}:$ANDROID_AVD_HOME
 ```
 
### 2.4) java install

```
sudo apt-get purge openjdk-\*   // delete old java
sudo apt-get autoremove  // delete autoremove
sudo apt-get install openjdk-8-jdk	// cordava requests java 8 version
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64  // for ubuntu 
java -version //see;  attention;  use single quotes "-"; do not use double quotes
not: sudo update-java-alternatives --list   // see java versions
not: sudo update-alternatives --config java // chose version 0,1,2 
```

manual java 8 installation : https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html

```
cd or mkdir "/usr/lib/jvm"
sudo tar -xvzf ~/Downloads/jdk-8u291-linux-x64.tar.gz
sudo gedit /etc/environment
//-------//
PATH="$PATH:/usr/lib/jvm/jdk1.8.0_291/bin:/usr/lib/jvm/jdk1.8.0_291/db/bin:/usr/lib/jvm/jdk1.8.0_291/jre/bin"
JAVA_HOME="/usr/lib/jvm/jdk1.8.0_291"
//----------//
source /etc/environment  // enable path ; end see this "env" command
sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_291/bin/java" 0
sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.8.0_291/bin/javac" 0
sudo update-alternatives --set java /usr/lib/jvm/jdk1.8.0_291/bin/java
sudo update-alternatives --set javac /usr/lib/jvm/jdk1.8.0_291/bin/javac
```

### 2.5) install gradle

```
sudo apt-get install gradle	// this use java
gradle -v // see
snap install gradle // snap install  7.0.2 version

```

## 3) installation methods

###  3.1)  use cordova pages

cordova create hello com.example.hello HelloWorld

###  3.2)  use vue-cli-plugin-cordova; 

https://github.com/m0dch3n/vue-cli-plugin-cordova

or use simple "vue-cli-plugin-cordova-simple"

```
vue create my-app
cd my-app
vue add cordova  (vue-cli-plugin-cordova)

 
npm run cordova-prepare

npm run cordova-serve-android # Development Android
npm run cordova-build-android # Build Android
npm run cordova-build-only-www-android # Build only files to src-cordova

npm run cordova-serve-browser # Development Browser
npm run cordova-build-browser # Build Browser
npm run cordova-build-only-www-browser # Build only files to src-cordova


eğer gerekirse bak
"cd src-cordova" 
"cordova platform add android"
and
"cordova requirements"
```

> build sırasında ':app:lintVitalRelease' hatası geliyorsa "build.gradle"
> dosyasında değişiklik yap

```
android {
        lintOptions {
            checkReleaseBuilds false
            abortOnError false
        }
    }
```

####  3.2.1) apt or aab create 
```
 node_modules\vue-cli-plugin-cordova\index.js  // edit line 221
 // for aab
 await cordovaBuild(platform)
 // for apk
 await cordovaBuild(platform,false)
```

### 3.3) Create cordova project with vue and webpack (old method )

method 3.3.1)  first installation
```
cordova create project_name
vue init webpack project_name
cd project_name
npm run dev
```

#### 3.3.2) edit config/index.js, and change dirname
```
build {
    index: path.resolve(__dirname,'../www/index.html'),
    assetRoot: path.resolve(__dirname,'../www'),
    assetSubDir: 'static',
    assetPublicPath: '',
}
```

#### 3.3.3)  edit index.html

```
<head>
<meta charset="utf-8">
<meta name="format-detection" content="telephone=no">
<meta name="msapplication-tap-highlight" content="no">
<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
<meta http-equiv="Content-Security-Policy" content="default-src *; style-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline' 'unsafe-eval'; media-src *; img-src 'self' data: content:; connect-src 'self' ws:;">
<title>GenuineImpact</title>
<script src="cordova.js"></script>
</head>
```

Write something on the `<body>`

#### 3.3.4)) nodjs test

```npm run dev```

#### 3.3.5) Platform add on cordova

```
cordova platform add android
ordova platform add browser
and add other platforms
cordova platform ls // see platforms
cordova platform rm android //  delete android platform
```

####3.3.6) cordova test

`cordova requirements`

#### 3.3.7) test cordova on borwser and android.

```
npm start //   port 8000 on html
cordova run browser --device
or
cordova run android --device  
```
```
cordova run android --nobuild // android device( phone, tablet or etc)  not connected 
cordova run android --device // debugging
cordova run --list // see emulates  // ex: Pixel_API_30 // see --target 
cordova run android --list  // android icin olanlar
cordova emulate android // test
cordova run android --emulator  --target=Pixel_API_30
```
#### 3.3.8) Build apk for Android

```
cordova build browser
cordova build android
```

## 4) create key on android 

```
keytool -genkey -v -keystore android.keystore -alias android.keystore -keyalg RSA -validity 20000
put the created "app-release-unsigned.apk" in your home directory
jarsigner -verbose -keystore android.keystore -signedjar android_signed.apk app-release-unsigned.apk android.keystore
```

keytool sytax
```
keytool 
  -genkey 
	-v 
	-keystore <keystoreName>.keystore 
	-alias <Keystore AliasName> 
	-keyalg <Key algorithm> 
	-keysize <Key size> 
	-validity <Key Validity in Days>
```

jarsigner sytax
```
jarsigner
  -verbose 
  -keystore (Signature file path example: android.keystore) 
  -signedjar (example of the address saved by the APK after signing: "android_signed.apk")  
  (Unsigned APK address path example file: "app-release_unsigned.apk") 
  (an alias for the signature file)
```					 

## 5) Post apk on phone 

connect phone and pc with usb cable

```
11.1) Cep telefonunda
11.2) /Ayarlar/Telefon hakkında/Yazılım bilgileri/Yapım numarası( 7 kez dokun )
11.3) /Ayarlar/Geliştirici seçenekleri/ Usb hata ayıklamayı etkin yap/  
11.4) usb kablosu ile pc ile telefonu birbirine bağla
11.5) telefonda Download dizini yoksa "my files" uygulamını yükle
11.6) Ubuntu da ----
11.7) cordova build android 
11.8) phone dizini gör ve apk yi kopyala 
11.9) cp /home/emr/hello/platforms/android/app/build/outputs/apk/debug/app-debug.apk phone/D
ownload/.
11.10) cep telefonund "my files" uygulamasında apk dizinine bak onu  çalıştır. Download dizinide gözükmez.. dikkat
```
https://developer.android.com/studio/debug/dev-options

## 6) android icon change 

resize image 

with online 

https://romannurik.github.io/AndroidAssetStudio/index.html

with code on console  //  convert commands;  install imagemagick 

sudo apt-get update

sudo apt-get install imagemagick -y


```
!/bin/bash

#  support  jpg,  png,  tiff

# ldpi    : 36x36 px
# mdpi    : 48x48 px
# hdpi    : 72x72 px
# xhdpi   : 96x96 px
# xxhdpi  : 144x144 px
# xxxhdpi : 192x192 px

convert emr.jpg -resize 36x36     emr01.ldpi.png
convert emr.jpg -resize 48x48     emr02.mdpi.png
convert emr.jpg -resize 72x72     emr03.hdpi.png
convert emr.jpg -resize 96x96     emr04.xhdpi.png
convert emr.jpg -resize 144x144 emr05.xxhdpi.png
convert emr.jpg -resize 192x192 emr06.xxxhdpi.png

```


edit config.xml

```
<platform name="android">
	<icon src="res/emr01.ldpi.png" density="ldpi" />
	<icon src="res/emr02.mdpi.png" density="mdpi" />
	<icon src="res/emr03.hdpi.png" density="hdpi" />
	<icon src="res/emr04.xhdpi.png" density="xhdpi" />
	<icon src="res/emr05.xxhdpi.png" density="xxhdpi" />
	<icon src="res/emr06.xxxhdpi.png" density="xxxhdpi" />
</platform>
```



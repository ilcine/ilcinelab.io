/* YEVKAN.P  */
/* Yazan Emrullah ILCIN emr@uludag.edu.tr 1996 Upgrade 2000 */
def var i as int.
def var j as int.
def var sonu as int format ">>>>>>>".
def var basi as int format ">>>>>>>".
def var topl as char format "x(132)".
def var   borc-top as dec format ">>>,>>>,>>>,>>>,>>>".
def var alacak-top as dec format ">>>,>>>,>>>,>>>,>>>".

def var brc-top as dec format ">>>,>>>,>>>,>>>,>>>".
def var alc-top as dec format ">>>,>>>,>>>,>>>,>>>".
def var yana-gercek as char format "x(4)".
def var yana-kont as char format "x(4)".
def var sec as char format "x" .
def var pp-ana as int format "99".
def var pp-adi as char format "x(72)".
for first muhack:
end.
set        "başlangıç nosu: " basi skip
	   "bitiş tarihi  : " sonu
	   with frame a
	   centered row 9 no-labels title "yevmiye listesi çekimi".
hide all no-pause.
borc-top = 0.
alacak-top = 0.
/* eger aradan cekim yapilacak ise matrah bastan itibaren toplatılır */
   for each muhyev where any1 <  basi.
      borc-top = borc-top + borc.
      alacak-top = alacak-top + alacak.
   end.
/* =================================================================== */

message "lütfen biraz bekleyiniz".
  if opsys = "msdos" then output to printer page-size 65.
		     else output to yevdok.prn page-size 65.
  for each  muhack where  a-any1 >   basi - 1  and a-any1 <  sonu + 1.


  topl = string(a-gun,"99,") + string(a-ayi,"99,") + string(a-yil,"9999").
   display "-----"  a-any1 format "99999"  "--------------------"
	 topl format "x(10)"
	 "------------------------------------------------------------"
	 + "----------------------------"
	 with no-label no-box width 132 .

/*  for each muhyev where any1 = a-any1 and any2 NE 0.  */
     for each muhyev use-index yevsira
      where any1 = a-any1 and any2 > 0 and yana > 0 .


  find muhpla where
	     p-ana = yana and p-alt1 = 0 and  p-alt2 = 0 no-error.

  yana-gercek = string(yana,"999") + string(ba-kod,"9").
  if yana-gercek <> yana-kont  then
      do:
      yana-kont = yana-gercek.
      if   brc-top > 0 and alc-top = 0   then

	    display space(10) pp-ana format "999" pp-adi format "x(71)"
		  brc-top   skip
		with frame borcframesi no-box no-label width 132.
      if alc-top > 0 and brc-top = 0  then
	    display space(35) pp-ana format "999" pp-adi format "x(71)"    space(3)
		 alc-top    skip
	    with frame alacakframesi no-box no-label width 132.
      brc-top = 0. alc-top   = 0.
      pp-adi = p-adi . pp-ana = p-ana.
      end.   /* --- if <> ün endi ---- */

  brc-top = brc-top + borc.
  alc-top = alc-top + alacak.
  borc-top = borc-top + borc.
  alacak-top = alacak-top + alacak.

   END.   /* muhyev  in endi */


	    display space(35) pp-ana format "999" pp-adi format "x(71)" space(3)
	     alc-top    skip
	    with frame alacakframesi1 no-box no-label  width 132.

      brc-top = 0. alc-top   = 0.
      yana-kont = " ".
      yana-gercek = " " .
      topl = STRING(a-gun,"99,") + string(a-ay,"99,") + string(a-yil,"9999,") .
      display topl format "x(10)"  "/" a-any1 format "99999"
	     "TAR/NO.LU FİŞ MUCİBİ"
	     with frame fislafiyaz no-label no-box width 132.
    display
     "----------------------------------------------------------------------" +
     "--------------------------------------------------------------"
      with frame cizgiyaz no-label no-box width 132.
    display space(68)  "Genel Yekün ---> " borc-top space(8) alacak-top
		 with frame gen-yek no-label no-box width 132.


   END.       /* muhack nin endi */


     output close    .
     if opsys = "unix" then unix cat yevdok.prn.

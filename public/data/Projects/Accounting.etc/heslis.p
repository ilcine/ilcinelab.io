/* HESLIS.P  */
/* Yazan Emrullah ILCIN emr@uludag.edu.tr 1996 Upgrade 2000 */

hide all no-pause.
prompt-for "başlangic hesap no:" p-ana with
	    frame ekrana no-label centered row 10
	    title "HESAP PLANI LİSTESİ".
hide all no-pause.
for each muhpla where p-ana > input p-ana - 1.
   display p-ana  label "ANA "
	   p-alt1 format ">>>" label "TALİ"
	   p-alt2 format ">>>>>" label "FERİ"
	   p-adi  label "Hesap plani adi" format "x(30)"
	   with no-label centered     down
	   title color messagess  "HESAP PLANI LISTESI" .
end.

/* BUTGEL.P  */
/* Yazan Emrullah ILCIN emr@uludag.edu.tr 1996 Upgrade 2000 */
/* Butce gelir listesi verir  */

/* butgir de 14 000 nolu ana hesaba  ve 14 105 nolu hesaba dogru bilgi gir */
def var ana-top     as dec format "->>>,>>>,>>>,>>>,>>9".
def var alt-top     as dec format "->>>,>>>,>>>,>>>,>>9".

def var o1-ay-top    as dec format "->>>,>>>,>>>,>>>,>>9".
def var tum1-ay-top  as dec format "->>>,>>>,>>>,>>>,>>9".
def var o2-ay-top    as dec format "->>>,>>>,>>>,>>>,>>9".
def var tum2-ay-top  as dec format "->>>,>>>,>>>,>>>,>>9".
def var o3-ay-top    as dec format "->>>,>>>,>>>,>>>,>>9".
def var tum3-ay-top  as dec format "->>>,>>>,>>>,>>>,>>9".

def var butce-tam     as dec format "->>>,>>>,>>>,>>>,>>9".
def var butce-oay     as dec format "->>>,>>>,>>>,>>>,>>9".
def var butce-yil     as dec format "->>>,>>>,>>>,>>>,>>9".


def var hangi-ay as  int.
def var basla as int format "99".
def var bitir as int format "99".



def var t-al  as char format "x(10)".
def var t-al0  as char format "x(10)".
def var t-al1 as char format "x(10)".
def var t1900 as int  format "9999".
def var t1901 as int  format "9999".
def var t1902 as int  format "9999".
def var t1903 as int  format "9999".

def var ciz as char format "x(132)".
def var ciz2 as char format "x(132)".
ciz2 = "====== =============================== =================== =================== =================== ===================".
ciz  = "------ ------------------------------- ------------------- ------------------- ------------------- -------------------".


basla=01. bitir = 99.
update  "Hangi Ayın Listesi :" hangi-ay format ">>" auto-return skip
	"Başlangiç Hesabı   :" basla     skip
	"Bitiş Hesabı       :" bitir
  with frame a no-underline  row 10 centered  no-label
  title "Gelir Bütçesi".
hide all no-pause.
/*
if opsys = "msdos" then output to printer page-size 155 paged.
		   else
		   output to butce.prn paged page-size 155.
*/
output to printer page-size 155 paged.
hide all no-pause.
  t1900 = int(substring(string(today),7,2)) + 1900  .
  t1901 = int(substring(string(today),7,2)) + 1900  .
  t1902 = int(substring(string(today),7,2)) + 1900  .
  t1903 = int(substring(string(today),7,2)) + 1900  .

if hangi-ay = 1  then do: t-al = "      OCAK". T-AL1 = "     31.01". end.
if hangi-ay = 2  then do: t-al = "     ŞUBAT". T-AL1 = "     28.02". end.
if hangi-ay = 3  then do: t-al = "      MART". T-AL1 = "     31.03". end.
if hangi-ay = 4  then do: t-al = "     NİSAN". T-AL1 = "     30.04". end.
if hangi-ay = 5  then do: t-al = "     MAYIS". T-AL1 = "     31.05". end.
if hangi-ay = 6  then do: t-al = "   HAZİRAN". T-AL1 = "     30.06". end.
if hangi-ay = 7  then do: t-al = "    TEMMUZ". T-AL1 = "     31.07". end.
if hangi-ay = 8  then do: t-al = "   AGUSTOS". T-AL1 = "     31.08". end.
if hangi-ay = 9  then do: t-al = "     EYLÜL". T-AL1 = "     30.09". end.
if hangi-ay = 10 then do: t-al = "      EKİM". T-AL1 = "     31.10". end.
if hangi-ay = 11 then do: t-al = "     KASIM". T-AL1 = "     30.11". end.
if hangi-ay = 12 then do: t-al = "    ARALIK". T-AL1 = "     31.12". end.
t-al0 = t-al.
tum1-ay-top = 0.
tum2-ay-top = 0.
tum3-ay-top = 0.
disp
     t-al t1900      "AYI KARŞILAŞTIRMALI GELİR BÜTÇESİ"  space(52)
			     page-number ".sayfa" skip(1)
     "Fasıl                                                " t1901
				"   " t-al0 t1902 "   " t-al1 t1903 skip
     "No.... Hesap Adı......................"
     "            Bütçesi" "          Tahsilatı"
     "          Tahsilatı" "             Bakiye"

	with frame ilkbas no-label no-box width 132 page-top.

ana-top = 0.
alt-top = 0.

for each muhpla
	      where p-alt1 >=  0 and p-alt2 >= 0 and  p-butce-gelir > 0
	      by p-ana descending.


	  /* ---- alttaki guruplari toplama almiyorum  */
	  /* --- cunki bu hesaplari topltarak buluyorum */

/* ----- ana gurup harici toplam alir- */
/* ------toplamlari alir ---- GERCEK HESAPLARI  ------------------------- */

	  if p-alt1 = 0  or
	   ( p-ana = 03 and p-alt1 = 100 )
	  /* ( p-ana = 14 and p-alt1 = 100 )  */ then do: pause 0 . end. else
	     do:
	     ana-top     = ana-top     + p-butce-gelir.
	     alt-top     = alt-top     + p-butce-gelir.
	     end.

/* --------- p-alt1 0 olanlar toplanir --(GAYRIMENKUL Satiri)------ */
       if p-alt1 = 0 then
	 do:
	 p-butce-gelir      = ana-top.
	 ana-top = 0.
	 alt-top = 0.
	 end.
/* ------p-alt1 100is (gerci yok olmasi lazim)--------  */

	  if ( p-ana = 03 and p-alt1 = 100 and p-alt2 = 0 )
	/*   ( p-ana = 14 and p-alt1 = 100 and p-alt2 = 0 ) */    then
	    do:
	    p-butce-gelir       = alt-top.
	    alt-top = 0.
	    end.


end. /* for each muhpla end  */


/* ================================================================ */

/*  ----- ACIKLAMA ------------- */
/*  muhpla daki p-alacak'a tum yilin toplamlari toplaniyor */
/*  muhpla daki p-borc 'a o aydaki toplamlar toplaniyor olarak kullanildi */


for each muhpla
	     where p-alt1 >= 0 and p-alt2 >= 0 and  p-butce-gelir > 0
	      by p-ana descending.


/* --------- muhpla altindakti 2.dongu  ------------------------ */

for each muhyev use-index kebir-index where
      yana = p-ana  and yalt1 = p-alt1 and any1 <> 1
      /* alttaki devreden hesaplar toplama alinmiyor */
      and  (  ( any1 <> 1 and yana <> 14 and yalt1 <> 105 )
	     or   ( any1 <> 1 and yana <> 1 and yalt1 <> 1 ) ).

if yana = 14 and yalt1 = 105 then butce-oay = butce-oay + alacak.

/* ---------- YIL TOPLAMI BASI (1)------------------- */

/* ----- 0 inci alt gurubu harici -- */
/* ------toplamlari alir ---- GERCEK HESAPLARI  ------------------------- */

	  if p-alt1 <> 0 then
	     do:
	     tum1-ay-top = tum1-ay-top  + alacak.    /*o aydaki ler toplanir */
	     tum2-ay-top = tum2-ay-top  + alacak.    /*o aydaki ler toplanir */
	     tum3-ay-top = tum3-ay-top  + alacak.    /*o aydaki ler toplanir */
	     end.
      /* ----------- YIL TOPLAMI SONU --!1)------- */

/* ----------- o ay harcamalari Toplami BASI (1)----------- */

  if hangi-ay = dayi then

  DO:   /* BUYUK İÇ DONGU BASİ */
/* ----- GAYRIMENKUL=ana gurubu ve SUURETIM,PERSONEL=alt gurubu harici -- */
/* ------toplamlari alir ---- GERCEK HESAPLARI  ------------------------- */

	  if p-alt1 <> 0 then
	     do:
	     o1-ay-top = o1-ay-top  + alacak.    /*o aydaki ler toplanir */
	     o2-ay-top = o2-ay-top  + alacak.    /*o aydaki ler toplanir */
	     o3-ay-top = o3-ay-top  + alacak.    /*o aydaki ler toplanir */
	     end.
       END. /* ---------BUYUK İÇ DONGU SONU  (ayi toplami) ------ */

    /* ------------- a ay harcamalari sonu ---(1) ---------- x */


    end. /* muhyev endi */



/* -------------- YIL TOPAMI BASI---(2)------------ */

/* --------- p-alt1 0 olanlar toplanir --(GAYRIMENKUL Satiri)------ */
       if p-alt1 = 0 then
	 do:
	 assign p-alacak = tum1-ay-top.
	 tum1-ay-top = 0.
	 tum2-ay-top = 0.
	 tum3-ay-top = 0.
	 end.


/* ------y-alt1 0 ise ()-------  */

/*
	  if ( p-ana = 01 and p-alt1 = 100 and p-alt2 = 0 ) or
	     ( p-ana = 14 and p-alt1 = 105 and p-alt2 = 0 ) then
	    do:
	    assign p-alacak    = tum2-ay-top.
	    tum2-ay-top = 0.
	    end.
 */

   /* ---------  ana hesaplari haricindekileri yazar */
	  if p-alt1 <> 0 then
	     do:
	     assign p-alacak = tum3-ay-top.
	     tum3-ay-top = 0.
	     end.


/* ---------------YIL TOPLAMI SONU ---(2)---------------- */


/* --------------- o AY toplami basi ---(2) ------------ */

/* --------- p,y-alt1 0 olanlar toplanir --(GAYRIMENKUL Satiri)------ */
       if p-alt1 = 0 then
	 do:
	 assign p-borc = o1-ay-top.
	 o1-ay-top = 0.
	 o2-ay-top = 0.
	 o3-ay-top = 0.
	 end.

   /* ---------  ana va alt hesaplari haricindekileri yazar */
	  if p-alt1 <> 0 then
	     do:
	     assign p-borc = o3-ay-top.
	     o3-ay-top = 0.
	     end.

   END.  /* muhpla  DONGUSU SONU  */

/* ----------- o ayin harcama toplami sonu ------------- */






for each muhpla where p-butce-gelir > 0 .
if p-alt1 = 0  then
     do:
     butce-tam = butce-tam + p-butce-gelir.
     butce-oay = butce-oay + p-borc.
     butce-yil = butce-yil + p-alacak.
     end.

if p-alt1 = 0  then  disp ciz2 with frame x16  no-label no-box width 132.

display  p-ana p-alt1 p-adi format "X(31)"
   /* 1 */  p-butce-gelir  format "->>,>>>,>>>,>>>,>>9"
   /* 2 */  p-borc   format "->>,>>>,>>>,>>>,>>9"
   /* 3 */  p-alacak  format "->>,>>>,>>>,>>>,>>9"
   /* 4 */  (p-butce-gelir - p-alacak )  format "->>,>>>,>>>,>>>,>>9"
	  with frame aa3 no-label no-box width 132.

  if p-alt1 = 0 then disp ciz with frame aa4 no-label no-box width 132.

end.
   display ciz  skip with frame bb1 no-label no-box width 132.


     display  "Toplam" "" format "X(31)"
   /* 1 */  butce-tam  format "->>,>>>,>>>,>>>,>>9"
   /* 2 */  butce-oay  format "->>,>>>,>>>,>>>,>>9"
   /* 3 */  butce-yil  format "->>,>>>,>>>,>>>,>>9"
   /* 4 */  (butce-tam - butce-yil )  format "->>,>>>,>>>,>>>,>>9"
	  with frame bb1 no-label no-box width 132.

     display ciz2   with frame bb12 no-label no-box width 132.

output  close.

/*
 unix cat butce.prn.
*/

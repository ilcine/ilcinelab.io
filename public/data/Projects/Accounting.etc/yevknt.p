/* YEVKNT.P  */
/* Yazan Emrullah ILCIN emr@uludag.edu.tr 1996 Upgrade 2000 */
def var h-borc-top  as decimal format "->>>,>>>,>>>,>>>,>>9".
def var h-alacak-top  as decimal format "->>>,>>>,>>>,>>>,>>9".
def var gen-borc   as dec format ">>>,>>>,>>>,>>>,>>9".
def var gen-alacak as dec format ">>>,>>>,>>>,>>>,>>9".
def var bas-no as int format ">>>>9".
def var bit-no as int format ">>>>9".
def var yevm-knt as char format "x".
update     "Başlangic yevmiye no..: " bas-no skip
	   "Bitiş yevmiye no......: " bit-no
	    with frame ilk1 no-label centered row 7 title " Yevmiye KONTROL".
hide frame ilk1 no-pause.
gen-borc = 0.
gen-alacak = 0.
for each muhack where
    muhack.a-any1 > bas-no - 1 and  muhack.a-any1 < bit-no + 1:
h-borc-top = 0.
h-alacak-top = 0.
  for each muhyev where muhyev.any1 =  muhack.a-any1 :
  h-borc-top = h-borc-top + borc.
  h-alacak-top = h-alacak-top + alacak.
  gen-borc = gen-borc + borc.
  gen-alacak = gen-alacak + alacak.
  end.
  if gen-borc = gen-alacak then yevm-knt= " ".  else yevm-knt = "H".
  display  a-any1 ".yevm/" a-gun a-ayi a-yil "....>" yevm-knt
	     h-borc-top " " h-alacak-top
	     with frame
	     ilk2 no-label 18 down
	     title "hesap kontrol uşlemi".
end.
  display 0 " Genel Toplam            " gen-borc " " gen-alacak with frame
		   ilk3 no-label overlay row 21 no-box .

/* YARSIF.P  */
/* Yazan Emrullah ILCIN emr@uludag.edu.tr 1996 Upgrade 2000 */
/* sifre girisi yapar */
/* ilk yaratilirken create muhprm. yaparak olusturdum tamami 1 kayittir  */
def var ilksifre as char format "x(5)".
update "Şifreyi gir.....:" ilksifre BLANK with frame ilk
       no-label centered row 10 title "ŞİFRE DEGİŞTİRME EKRANI".
for first muhprm where sifre = ilksifre:
  do transaction:
  update "Yeni şifreyi gir:" muhprm.sifre BLANK format "x(5)"
	  with frame dosyasifresi no-label centered row 13
	  title "  YENİ ŞİFREYİ EKRANI  ".
  end.
  disp "işleminiz başarılı (şifrenizi unutmayın)".
  pause 2.
  undo  , return.
end.
disp "var olan şifreyi yanlis girdiniz     (şefreleri degiştirme hakkınız yok)".
pause 2.

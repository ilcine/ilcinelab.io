/* MIZACK.P  */
/* Yazan Emrullah ILCIN emr@uludag.edu.tr 1996 Upgrade 2000 */
/*        aciklamali mizan print******   */
def var i as int.
def var j as int.
def var bas-ana as int format ">>".
def var son-ana as int format ">>".
def var bas-tali as int format ">>>".
def var son-tali as int format ">>>".
def var bas-feri as int format ">>>>>".
def var son-feri as int format ">>>>>".
def var topl as char format "x(78)".
def var   borc-top as dec   format ">>,>>>,>>>,>>>,>>9".
def var alacak-top as dec   format ">>,>>>,>>>,>>>,>>9".
def var bak-borc as dec     format ">>,>>>,>>>,>>>,>>9".
def var bak-alacak as dec   format ">>,>>>,>>>,>>>,>>9".
def var bas-ay as int format ">>".
def var bit-ay as int format ">>".
def var sec as char format "x" .
set "başlangıç hesap nosu: " bas-ana bas-tali bas-feri  skip
    "Başlangıç ayin giriniz.:" bas-ay skip
    "Bitiş ayını giriniz....:" bit-ay skip
	   with frame a
	   centered row 9 no-labels title "Açiklamali Mizan".
     if son-ana = 0 and son-tali = 0 and son-feri = 0 then
     son-ana = bas-ana. son-tali = bas-tali. son-feri = bas-feri.
hide all no-pause.
if opsys = "msdos" then output to printer page-size 55 paged.
		   else
		   output to mizack.prn paged page-size 55.

for each muhpla where
 p-ana = bas-ana  and p-alt1 = bas-tali  and  p-alt2 = bas-feri :

 /* p-ana < son-ana + 1 and p-alt1 < son-tali + 1 and  p-alt2 < son-feri + 1:
  */
    borc-top = 0.
    alacak-top = 0.
form header
     "Hesap nosu :" p-ana p-alt1 p-alt2  page-number to 70 ".sayfa" skip
     "Hesap Adı  :" p-adi skip
     "-------------------------" skip
     "YEVM-TARİH FİŞNO A Ç I K L A M A           " +
     "     B O R Ç         A L A C A K   "
     skip
     "========== ===== ========================= " +
     "================== =================="
     skip
     "                 Nakli yekün.............>" borc-top alacak-top
     with frame hdr page-top no-box.
     /* ============= nakli yekün toplama ================== */
     for each muhyev use-index kebir-index where
      yana = muhpla.p-ana and yalt1 = p-alt1 and yalt2 = p-alt2 and
	 ( borc > 0 or alacak > 0 )  and
	 ( dayi <  bas-ay ).
      borc-top = borc-top + borc.
      alacak-top = alacak-top + alacak.
     end.
/*     disp borc-top alacak-top.
     */
     /* ====================================================== */


 for each muhyev use-index kebir-index where yana = muhpla.p-ana
	  and p-alt1 = yalt1  and p-alt2 = yalt2
	     and ( borc > 0 or alacak > 0 )
	    and ( dayi > bas-ay - 1 and dayi < bit-ay + 1 ).
   view frame hdr.
   /*
   form "----------------------------------------------------------------------"
     with frame sonuu page-bottom.
   view frame sonuu.
   */
   topl =
    string(dgun,"99,") + string(dayi,"99,") + string(dyil,"9999") + " " +
    string(any1,"99999").

	  display  topl format "x(16)" acik format "x(25)"
		    borc format   ">>,>>>,>>>,>>>,>>9"
		    alacak format ">>,>>>,>>>,>>>,>>9"
		    with frame br1 no-label no-box 16  down.


	borc-top = borc-top + borc.
	alacak-top = alacak-top + alacak.


     end. /* muhyev */
  /*   hide all no-pause.    */

     display
     "-------------------------------------------------------"   +
	"-------------------------" skip
	   with frame son1 no-label no-box down row 18 overlay.
     display   space(21) "Cari Toplam :        "  borc-top  alacak-top   skip
		       with frame son1 no-box no-label down  row 18 overlay.
     if borc-top > alacak-top then bak-borc = borc-top - alacak-top.
     if alacak-top > borc-top then bak-alacak = alacak-top - borc-top.
     if borc-top = alacak-top then
		    do:
		    bak-alacak = 0. bak-borc = 0.
		    end.
     display space(21)   "Bakiye      :        "  bak-borc bak-alacak skip
		       with frame son1 no-box no-label down row 18 overlay.

  /*   pause .    */
  end. /* muhack */

     output close    .
     if opsys = "unix" then unix cat mizack.prn.

/* YARFIR.P  */
/* Yazan Emrullah ILCIN emr@uludag.edu.tr 1996 Upgrade 2000 */
/* Firma girisi yapar */
/* ilk yaratilirken create muhprm. yaparak olusturdum tamami 1 kayittir  */
def var ilksifre as char format "x(5)".
update "Şifreyi gir.....:" ilksifre BLANK with frame ilk
       no-label centered row 2 title "Firma adi digiştirme   ".
for first muhprm where sifre = ilksifre:
  do transaction:
  update  "Firma adi.......:" muhprm.firmaadi skip
	  "Firma adresi (1):" firmaadres1 skip
	  "firma adresi (2):" firmaadres2 skip
	  "Şehir...........:" sehir  skip
	  "telefonlar......:" telefon  skip
	  with frame dosyasifresi no-label centered row 7
	  title "  Adres Bilgisi Degişiklik Ekranı  ".
  end.
  disp "işleminiz başarılı ".
  pause 2.
  undo  , return.
end.
disp "var olan şifreyi yanlis girdiniz  (   Firma adını degiştiremezsiniz    )".
pause 2.

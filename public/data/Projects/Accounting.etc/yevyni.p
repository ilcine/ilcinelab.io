/* YEVYNI.P  */
/* Yazan Emrullah ILCIN emr@uludag.edu.tr 1996 Upgrade 2000 */
/* -genel muhasebe paket programı ana çatısı (yevm giriş) -%---- */
DEFINE VARIABLE ddate AS DATE.
def var ilksifre as char format "x(5)".
def var h-borc-top  as decimal format "->>>>,>>>,>>>,>>9".
def var h-alacak-top  as decimal format "->>>>,>>>,>>>,>>9".
def var hafiza-gun as int format ">>".
def var hafiza-ayi as int format ">>".
def var hafiza-yil as int format ">>>>".
def var emr2 as int format ">>>>>".
def var fonk-acik as char format "X(40)".
REPEAT:
for last muhyev.
end.
prompt-for muhyev.any1 with frame ekr1 no-label
    title "yevm no"

     EDITING:  /* -------- great edit ---- */
     READKEY.
     APPLY LASTKEY.
      /* -------------------------- */
	IF GO-PENDING AND input any1 = 0 then
	DO:
	  MESSAGE " yevmiye no  0 OLAMAZ ".
	  NEXT.
	END. /* IF GO-pending in ENDi */
     /* ------------------------------------  */
     if go-pending and
	  keyfunction(lastkey) = "RETURN" and
		 input any1 > muhyev.any1 + 1 then
	  DO:
	    message "yevmiye planinda atlama yapılıyor dogrusu "
	    muhyev.any1 + 1  "dir " .

	    message "return".
	    next.
	  END. /* if go_pend in END i */
     END.  /* editin */
    /* ------------------------------------- */



   find muhyev where any1 = input any1  and any2 = 0 no-error.
	if  not available muhyev then
	      DO:   /* ----  1202 no li do --- */
		create muhyev.
		message "ilk yevmiye nosu giriliyor.".
		dgun = int(substring(string(today),4,2)).
		dayi = int(substring(string(today),1,2) ).
		dyil = int(substring(string(today),7,2)) + 1900 .
		acik = string(dgun,"99,") + string(dayi,"99,") +
		       string(dyil,"9999") + " / " +
		       string(input any1,"999999999") +
		       " TAR/NO.LU FİŞ MUCİBİ".
		ba-kod = " ".
		update "Yevmiye.Tarh:"
		dgun auto-return
		dayi auto-return
		dyil auto-return       skip
		"Gen açiklama:" acik   with frame acikgir12
		row 1 column 1  no-labels     overlay
		title " Genel açiklama ve yevmiye kaydi girişi"
		    EDITING:
		       readkey.
		       apply lastkey.
		       if frame-field = "acik" then
		       ddate = date(input dayi,input dgun,input dyil).
		    END. /* editin end'i dir */
	       assign any1 = input any1 any2 = 0.
	       hafiza-gun = input dgun.
	       hafiza-ayi = input dayi.
	       hafiza-yil = input dyil.
	       END. /* if not  available nin endi 1202 */

      if available muhyev then
	  do:
	  hafiza-gun = dgun.
	  hafiza-ayi = dayi.
	  hafiza-yil = dyil.
	  end.

	    /* ---- aciklamaya 0 girme --- */

	    find muhack where a-any1 = input any1 no-error.
	    if not available muhack then
	       do:
		 create muhack.
		 a-any1 = input any1.
		 a-acik = input acik.
		 a-gun = input dgun.
		 a-ayi = input dayi.
		 a-yil = input dyil.
		 assign a-any1 = input any1.
	       end.  /* not available thru nin END i */

      /* ---------- bu bolüm alt procudure degildir ----*/
	 /* şifre kontrolu yapar */
	     if muhack.a-kod = "*" then
		 do:
		 message "bu yevmiye kayidi kapatılmıs".
		 message "Degiştirmek için şifre giriniz.." update ilksifre.
		   for first  muhprm :
		   if sifre <> ilksifre then
				do:
				message "sifreniz yanlis".
				undo , return.
				end.
		   end.
		 end.



/* -------hesaptaki matrahi bulma işlem---------------------------- */
h-borc-top = 0.
h-alacak-top = 0.
emr2 = 0.
for each   muhyev   where
	  muhyev.any1 = input muhyev.any1 and muhyev.any2 > 0.
	h-borc-top   = h-borc-top   + borc.
	h-alacak-top = h-alacak-top + alacak.
	emr2 = muhyev.any2.
end.   /* for each muhyev in endi */

if h-borc-top ne h-alacak-top then
message "BORC ve ALACAK EŞİTLİGİ YOK !!!                       "
		  h-borc-top h-alacak-top.


REPEAT:
emr2 = emr2 + 1.
find
  muhyev where  any1 = input muhyev.any1 and any2 = emr2 no-error no-wait.
if not available muhyev then do:
   disp emr2 with frame ekr2 col 10 row 1  no-label title "sira".
   disp h-borc-top h-alacak-top with frame ekr3 col 46 row 3 no-label no-box.
   create muhyev.
   update
     yana  format "999" auto-return
     yalt1 FORMAT "999" auto-return
     yalt2 format "99999" auto-return
     go-on(CTRL-D CTRL-A )
     with  frame ae   down
     overlay
     no-labels no-underline  no-box.

     if lastkey = keycode("CTRL-A") then
		      message "fonksiyon aciklamasi gir" update fonk-acik.
     if fonk-acik >
		       "" then acik = fonk-acik.
     find FIRST muhpla where  MUHPLA.P-ANA = INPUT YANA
				AND  P-ALT1 = input YALT1
				AND  P-ALT2 = input yalt2
				NO-ERROR NO-WAIT.
		  MESSAGE P-ADI.




     message "aciklama gir " update acik.
     display  acik format "x(31)"  with frame ae.
     update
     borc   auto-return format ">>>>,>>>,>>>,>>>"
     alacak auto-return format ">>>>>,>>>,>>>,>>>"
     with frame ae
	       EDITING:
	       READKEY.
	       APPLY LASTKEY.
	       IF go-pending and  INPUT borc >  0 and INPUT alacak > 0 then
		   DO:
		   message "hem borca hem alacaga deger girilmis".
		   next.
		   end.
      if borc > alacak then ba-kod = "1".  else  ba-kod = "2".
      if borc = alacak then ba-kod = " ".
     END.  /* edit */
   end. /* if  */

 dgun = hafiza-gun.
 dayi = hafiza-ayi.
 dyil = hafiza-yil.

 assign muhyev.any1 = input any1 muhyev.any2 = emr2.


h-borc-top   = h-borc-top   + input borc.
h-alacak-top = h-alacak-top + input alacak.

       END. /* repeat */
     end. /* repeat = */

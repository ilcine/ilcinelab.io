/* YEVDUZ.P  */
/* Yazan Emrullah ILCIN emr@uludag.edu.tr 1996 Upgrade 2000 */
DEFINE VARIABLE ddate AS DATE.
def var haf-gun as int format ">>".
def var haf-ayi as int format ">>".
def var haf-yil as int format ">>".
def var ilksifre as char format "x(5)".
REPEAT:
prompt-for muhack.a-any1 with frame ilk1 no-label title "baş fisno gir".
for each muhack where muhack.a-any1 = input a-any1.
 /* şifre kontrolu yapar */
	     if muhack.a-kod = "*" then
		 do:
		 message "bu yevmiye kayidi kapatılmıs".
		 message "Degiştirmek için şifre giriniz.." update ilksifre.
		   for first  muhprm :
		   if sifre <> ilksifre then
				do:
				message "sifreniz yanlis".
				undo , return .
				end.
		   end.
		 end.



     haf-gun = muhack.a-gun.
     haf-ayi = muhack.a-ayi.
     haf-yil = muhack.a-yil.
    update "yevmiye tarihi" a-gun a-ayi a-yil skip
	   "aciklama      " a-acik format "x(40)"
	   with frame ilk2 no-label no-box col 17
	   EDITING:
		       readkey.
		       apply lastkey.
		       if frame-field = "a-acik" then
		       ddate = date(input a-ayi,input a-gun,input a-yil).
	    END. /* editin end'i dir */
       /* yevmiye tarihi degiştiginde muhyev deki tüm kayitlari deiştirir */
       if  haf-gun NE input a-gun or
	   haf-ayi NE input a-ayi or
	   haf-yil NE input a-yil then
	   do:
	   message "Yevmiye Tarini degiştirmeyecekseniz (ESC) tusuna basınız".
	   for each muhyev where  any1 = muhack.a-any1.
	     dgun = input a-gun.
	     dayi = input a-ayi.
	     dyil = input a-yil.
	   end.  /* for each end */
	   end.
  prompt-for muhyev.any2 validate(any2 <>  0, " 0 girilmez" ) with frame  ilk1.

  for each muhyev where muhyev.any1 = muhack.a-any1 and
	       muhyev.any2 >  input muhyev.any2 - 1   :
  disp muhyev.any2 with frame ilk1.
   update yana yalt1 yalt2
       /*  acik format "x(40)" borc alacak    */
	 with frame ilk3 no-label down
	       title "duzeltme ekranı".
     find FIRST muhpla where  MUHPLA.P-ANA = INPUT YANA
				AND  P-ALT1 = input YALT1
				AND  P-ALT2 = input yalt2
				NO-ERROR NO-WAIT.
		  MESSAGE P-ADI.

       message "aciklama gir " update acik .
       display acik format "x(28)" with frame ilk3.
       update borc alacak with frame ilk3
	       EDITING:
	       READKEY.
	       APPLY LASTKEY.
	       IF go-pending and  INPUT borc >  0 and INPUT alacak > 0 then
		   DO:
		   message "hem borca hem alacaga deger girilmis".
		   next.
		   end.
	       END.   /* edit */
      if borc > alacak then ba-kod = "1".  else  ba-kod = "2".
      if alacak = borc then ba-kod = " ".
   end.    /* muhack */
   end.   /* muhyev */
   end.  /* repeat  */

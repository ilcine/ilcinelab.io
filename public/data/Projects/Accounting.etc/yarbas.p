/* YARBAS.P  */
/* Yazan Emrullah ILCIN emr@uludag.edu.tr 1996 Upgrade 2000 */
/* yilbasi islemleri */
def var ii    as dec format ">>>,>>>,>>>,>>>,>>>".
def var jj    as dec format ">>>,>>>,>>>,>>>,>>>".
def var ii-tp as dec format ">>>,>>>,>>>,>>>,>>>".
def var jj-tp as dec format ">>>,>>>,>>>,>>>,>>>".
def var sirasay as int.
def var sonno as int.
def var ilksifre as char format "x(5)".
def var alsifre as char format "x(5)".
/*
def workfile lafvar
      field lp-any1      as int format "99999"
      field lp-any2      as int format "99999"
      field lp-ana       as int format "999"
      field lp-alt1      as int format "999"
      field lp-alt2      as int format "99999"
      field lmiz-brc     as dec format ">>>,>>>,>>>,>>>,>>9"
      field lmiz-alc     as dec format ">>>,>>>,>>>,>>>,>>9".

*/

      /* ----------- s t o p ---------------- */


update "Şifreyi gir.....:" ilksifre BLANK with frame ilk
       no-label centered row 5 title
       "Y ı l b a ş ı     İ ş l e m l e r i ".
     for first  muhprm.
     alsifre = sifre.
     end.
     if alsifre <> ilksifre then leave.

for each yarson.
delete yarson.
end.


/* __program test edildi dogru calisiyor istenildiginde stop 'u kaldir ___ */
Message "Lütfen Bekleyiniz...".
for last muhyev.
sonno = any1.
end.
sirasay = 0.
for each muhyev where any1=sonno and any2 > 0 and alacak > 0 .
create yarson.
lp-any1 = 1.
	sirasay = sirasay + 1.
lp-any2 = sirasay .
lp-ana = yana.
lp-alt1 = yalt1.
lp-alt2 = yalt2.
lmiz-brc = borc.
lmiz-alc = alacak.
ii-tp = ii-tp + borc.
jj-tp = jj-tp + alacak.
/* disp  lafvar. */
end.
for each muhyev where any1=sonno and any2 > 0 and borc  > 0 .
create yarson.
lp-any1 = 1.
	sirasay = sirasay + 1.
lp-any2 = sirasay .
lp-ana = yana.
lp-alt1 = yalt1.
lp-alt2 = yalt2.
lmiz-brc = borc.
lmiz-alc = alacak.
ii-tp = ii-tp + borc.
jj-tp = jj-tp + alacak.
/* disp  lafvar. */
end.



   /* :::::: islemde hata var .. tüm dosyalari sildi ama yaratmadi */


disp " toplama bitti...> " ii-tp jj-tp with frame eef no-label
	   title "kontrol tablosu".
pause .001.

message " yevmiyeler siliniyor (1)" .
for each muhyev.
delete muhyev.
end.
message "Yevmiyeler siliniyor (2)".

for each muhack.
delete muhack.
end.

    find muhyev where any1 = 1  and any2 = 0 no-error.
    if  not available muhyev then
	    do:
	    create muhyev.
		any1 = 1.
		any2 = 0.
		dgun = 1.
		dayi = 1.
		dyil = int(substring(string(today),7,2)) + 1900 .
		ba-kod = "0".
		update "Açılış kaydi Yevmiye.Tarh:"
		dgun auto-return
		dayi auto-return
		dyil auto-return       skip
		"Açılış Gen açiklama:" acik   with frame acikgir12
		row 15 column 1  no-labels     overlay
		title " Genel açiklama ve yevmiye kaydi girişi".
	    assign any1 = 1 any2 = 0.
	    end.


	    find muhack where a-any1 = 1 no-error.
	    if not available muhack then
	       do:
	       create muhack.
	       a-any1 = 1.
	       a-acik = input acik.
	       a-gun = input dgun.
	       a-ayi = input dayi.
	       a-yil = input dyil.
	       assign a-any1 = 1.
	       end.
/* ______________ BORC yazdiriyor ________________ */
    for each yarson where lmiz-alc > 0.
    find muhyev where any1 = lp-any1  and any2 = lp-any2 no-error.

    if  not available muhyev then
	    do:
	    create muhyev.
	    dgun = input dgun.
	    dayi = input dayi.
	    dyil = input dyil.
	    acik = input acik.
	    ba-kod = "1".
	    BORC = lmiz-alc.
	    Alacak = 0.
	    yana = lp-ana.
	    yalt1 = lp-alt1.
	    yalt2 = lp-alt2.
	    assign any1 = lp-any1 any2 = lp-any2.
	    end.
    end.


/* ______________ alacak yazdiriyor ________________ */
    for each yarson where lmiz-brc > 0.
    find muhyev where any1 = lp-any1  and any2 = lp-any2 no-error.

    if  not available muhyev then
	    do:
	    create muhyev.
	    dgun = input dgun.
	    dayi = input dayi.
	    dyil = input dyil.
	    acik = input acik.
	    ba-kod = "2".
	    BORC = 0.
	    Alacak = lmiz-brc.
	    yana = lp-ana.
	    yalt1 = lp-alt1.
	    yalt2 = lp-alt2.
	    assign any1 = lp-any1 any2 = lp-any2.
	    end.
    end.

Message "İşleminiz basari ile sonuclandi ".
pause.

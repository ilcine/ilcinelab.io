# ldapServer

LDAP (Lightweight Directory Access Protocol) running on TCP / IP
used to query and modify index services
The application is a layered protocol.
This protocol is used as an Active Directory on the Microsoft side.
LDAP server, SSO (Single Sign On) Provides access to systems connected with single password usage.
The network holds data in many fields such as device information, User address information, and makes it accessible to users.
It is a database storage system that allows authorizing users to service.
In these notes, OpenLdap system will be installed on Ubuntu system.

-

LDAP (Lightweight Directory Access Protocol) TCP/IP üzerinde çalışan 
indeks servislerini sorgulama ve değiştirme amacıyla kullanılan
uygulama katmanlı protokoldur.
Bu protokol Microsoft tarafında Active Directoryolarak kullanmatadır.
LDAP sunucusu, SSO (Single Sign On) Tek şifre kullanımı ile bağlı sistemlere erişim sağlar.
Ağ cihaz  bilgileri, Kullanıcı adres bilgileri gibi birçok alanda veri tutar ve kullanıcıların erişimine açar.
Hizmet alan kullanıcılara yetkilendirmeye izin veren bir veri tabanı depo sistemidir. 
Bu notlarda Ubuntu sistemi üzerinde OpenLdap sistemi kurulacaktır.
This document creates LDAP Directory Server (Bu uygulama ldap directory server oluşturur)

_Emrullah İLÇİN_

 
## Server platform (Uygulamanın çalıştığı ortam)
    Server: Ubuntu 16.04 64bit;
    OpenLdap V2.4.2   
## Used domain (Kullanılan domain)
    ilcin.name.tr

## Ldap Installation (Kurulum)

1.  `hostname ldap.ilcin.name.tr`  # add host and domain (komutu ile host ve domain ver. ldap kurulum sırasında bu domaine bakar) `hostname -d` see host and domain (Tanımlanan domain i görmek için)
1.  `apt -y install slapd ldap-utils` # install app and extension (uygulamayı ve eklerini kur.) and enter  `Administrator password: & Confirm password:` with admin user;
1.  slapcat  // see first config (ile bak ilk ayarları göreceksin).
1.  `ldapsearch -x -LLL -H ldap:/// -b dc=ilcin,dc=name,dc=tr dn`  # test
1.  `dpkg-reconfigure slapd` if there is a problem run this command; it re-installs (eğer sorun varsa bu komutu kullan; kurulum yenilenir)

1.  Create OU: 
    * example `base.ldif` file and change OU people ve groups; (base.ldif i yrat ve people ve groups organization Unit leri kendine göre değiştir)
    ```
    dn: ou=people,dc=ilcin,dc=name,dc=tr
    objectClass: organizationalUnit
    ou: people

    dn: ou=groups,dc=ilcin,dc=name,dc=tr
    objectClass: organizationalUnit
    ou: groups
    ```
    `ldapadd -x -D cn=admin,dc=ilcin,dc=name,dc=tr -W -f base.ldif`  # Domain Component (DC) is defined, (DC tanımlanır)

1.  Create User:
    `Generate password for user (Kullanıcı için bir şifre oluştur)`
    * `slappasswd`  # see at in action `{MD5}xxxxxxxxxxxxxxxxxxx`; komutu çalıştır yaratılanı gör. ( Use CRYPT,SSHA or etc if you want to) 
    *  Create an example 'emr.ldif' file and replace it by itself and enter the created password (Alttaki örneği kendiğne göre değiştir ve yaratılan şifreyi kullanarak emr.ldif dosyası oluştur)
   
    ```
    dn: cn=emr,ou=people,dc=ilcin,dc=name,dc=tr
    cn: emr
    gidnumber: 2001
    givenname: emr
    homedirectory: /home/emr
    loginshell: /bin/sh
    objectclass: inetOrgPerson
    objectclass: posixAccount
    objectclass: top
    sn: ilcin
    uid: emr
    uidnumber: 2001
    userpassword: {SSHA}xxxxxxxxxxxxxxxxxxx
    ```
    * `ldapadd -x -D cn=admin,dc=ilcin,dc=name,dc=tr -W -f emr.ldif`  # emr user occurs (emr kullanıcısı oluşur)

1.  Search:

    `Public Search: anyone can ask (herkez sorabilir)`

    * `ldapsearch -x -b "ou=people,dc=ilcin,dc=name,dc=tr"`  # see ou people ve domain ( ou, people ve domaini gör)
    * `ldapsearch -x -b "cn=emr,ou=people,dc=ilcin,dc=name,dc=tr"` #see cn record, ( cn kaydını gör)
    * `ldapsearch -x -b "ou=people,dc=ilcin,dc=name,dc=tr" -s sub 'uid=emr'` # see uid
    * `ldapsearch -x -b dc=ilcin,dc=name,dc=tr -LLL -H ldap:///`  # see full domain
    * `ldapwhoami -x -W -D cn=emr,ou=people,dc=ilcin,dc=name,dc=tr` # so ask (böylede sor).

    `Private Search:` 

    * `ldapsearch -x -b "ou=people,dc=ilcin,dc=name,dc=tr" -D "cn=emr,ou=people,dc=ilcin,dc=name,dc=tr" -W ` # admin can ask ( admin kullanıcısı sorar)

1.  For remove: (Silmek için) 
    * `ldapdelete -x -h localhost -D "cn=admin,dc=ilcin,dc=name,dc=tr" "cn=emr,ou=people,dc=ilcin,dc=name,dc=tr" -W`

1.  Attribute ( add and modify attribute)

    `Add attribute:`
    * created `add-attribute.ldif` file with new `gecos` attribute; (Yeni `gecos` attributesi yaratmak için `add-attribute.ldif` i kullan) 
    
    ```
    dn: cn=emr,ou=people,dc=ilcin,dc=name,dc=tr
    changetype: modify
    add: gecos
    gecos: Guest User
    ```
    * `ldapmodify -D "cn=admin,dc=ilcin,dc=name,dc=tr" -W -x -f add-attribute.ldif`  # run ok. 

    `Change attribute:`

    *created `change-attribute.ldif` file with replace `sn` attribute; (sn attributesini değiştirmek için örnek `change-attribute.ldif` kullan)
 
    ```
    dn: cn=emr,ou=people,dc=ilcin,dc=name,dc=tr
    changetype: modify
    replace: sn
    sn: EMRILCIN
    ```
    * `ldapmodify -D "cn=admin,dc=ilcin,dc=name,dc=tr" -W -x -f change-attribute.ldif`  # run ok.

##  Pam and nsswitch install

   `apt install ldap-auth-client nscd # çalıştır config bilgilerini gir.` 
***
   ```
   - yes  # configrasyon y/n
   - ldap://localhost/   # ldap url   
   - dc=ilcin,dc=name,dc=tr # dn record
   - 3   # ldap version 
   - yes # root admin password 
   - no # ldap db login 
   - cn=admin,dc=ilcin,dc=name,dc=tr  # root authority for ldap
   - password # enter password
   - md5 # chose crypt or etc
   ```
***
   * `dpkg-reconfigure ldap-auth-config` # re config ( eğer gerekirse konfig tekrarı)
   * `auth-client-config -t nss -p lac_ldap`   # updates nsswitch.conf and pam configuration 

   `create this file;  /usr/share/pam-configs/mkhomedir  #  authority, own, skel and etc for the user (kullanıcı için örnek chown, skel vb yetkiler olşur)`

*** 
    Name: activate mkhomedir
    Default: yes
    Priority: 900
    Session-Type: Additional
    Session:
            required pam_mkhomedir.so umask=0022 skel=/etc/skel

   * pam-auth-update  # run and chose `mkhomedir`

   * /etc/init.d/nscd restart  # nscd run

##  Testing:
   * `getent passwd` or `getent shadow` or  `getent group` # see them
   * create `ldapadd` user for guest user # guest kullanıcı için `ldapadd` ile kayıt oluştur ) 
   * test 1: Connect to server with ssh, and login with guest; you will see that your /home/guest is created. (`guest` hesabı ile ssh yaparak login ol `/home/guest` yaratılır. (admin'in `adduser guest` ile kullanıcı yaratmasına gerek kalmaz) 
   * test 2: `su - guest` # see  `/home/guest` and  see `id` output "uid,gid ve groups" ( su ile guest ol, /home/guest i gör `id` komutu ile uid,gid ve guesti gör)
   * test 3: `ldapsearch -x -b "ou=people,dc=ilcin,dc=name,dc=tr" -s sub 'uid=emr'` # test with ldap

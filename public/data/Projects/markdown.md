# markdown sign

|Markdown | Output|
|---------|-------|
`#,##,###,####,#####`     | `html: h1,2,3,4,5`
=== text underline     | html: h1
--- text underline     | html: h2 
\*\*bold\*\*   | **bold**
\_\_bold\_\_   | __bold__
\*italic\*      | *italic*
\_italic\*      | _italic_
\*\*\*bold and italic\*\*\* | ***bold and italic***
\_\_\_bold and italic\_\_\_ | ___bold and italic___
\~\~Strikethrough\~\~ |~~Strikethrough~~ 
\> create a blockquote | <blockquote><p> create a blockquote</p></blockquote>
finish line two spaces | new line
\\           | escape
`1.`             | html: ol li
\- or \*        | html: ul li
\- under tab \-  | html: "ul" in "ul"
\`\`\` code \`\`\` | ``` code ```
\` code \`         | ` code `
\*\*\* , \-\-\- or ___ | ___
\[Google\]\(http://google.com) | [Google](http://google.com) link
\<fake@example.com\> | <fake@example.com>
\*\*\[GITLAB\]\(https://gitlab.com)** | **[GITLAB](https://gitlab.com)**
`![image](/a.jpg "a image")`  | image
\-\-\|\-\-   | table
\- \[x\] Write | - [x] Write
`:bulb:` |:bulb: [Gitlab emoji](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md#emoji) [Github emoji ](https://gist.github.com/rxaviers/7360908)
\<pre\>abc\<\/pre\> | html text

emoji other link |  [emoji ] (https://github.com/jzeferino/AllGithubEmojis)


You can use this html tag in md

h1 h2 h3 h4 h5 h6 h7 h8 br b i strong em a pre code img tt div ins del sup sub p ol ul table thead tbody tfoot blockquote dl dt dd kbd q samp var hr ruby rt rp li tr td th s strike summary details caption figure figcaption abbr bdo cite dfn mark small span time wbr

[markdown on github] (https://github.com/emn178/markdown/blob/master/README.md)



# Apache Cordova (for windows 10)

## links
```
https://evothings.com/doc/build/cordova-install-windows.html
https://tomspencer.dev/blog/2017/05/30/a-guide-to-installing-cordova-on-windows-10/
https://www.studytonight.com/apache-cordova/tools-installation
https://www.javatpoint.com/cordova-installation // full özellikler var
https://www.teknolojioku.com/guncel/tum-cmd-komutlari-5a28f27b18e540630d1cdea0
```

## 1)required for cordova


### 1.1) install git and grade
```
Http://git-scm.com 
https://github.com/git-for-windows/git/releases/download/v2.43.0.windows.1/Git-2.43.0-64-bit.exe
```

## 1.2) install node 
```
http://nodejs.org  (v:21.6.1)
```

## 1.3) install java  // versiyon 8 i
```
https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html
Java SE JDK'nın (SE = Standart Sürüm) son sürümünü Oracle'dan indirin:  (Windows x86) jdk-8u291-windows-x64.exe
www.oracle.com/technetwork/java/javase/downloads/ . 
```

### 1.3.1) java path

``` 
PATH
* sysdm.cpl
  or    
  /windows başlat/Denetim Masasi/System güvenlik or (system)/Gelişmiş Sistem Ayarlar/Ortam Değişkenleri/
  /"Sistem değişkenleri"/yeni/
-------------------------------
değişken adı: "JAVA_HOME"
değişken değeri: "C:\Program Files\Java\jdk-11.0.17\"  

  /"sistem değişkenleri"/path/düzenle/yeni/ 
   
add "%JAVA_HOME%\bin"

Test:
(cmd) echo %PATH:;=&echo.%

(cmd) javac -version   // see javac jdk-11.0.17

```

## 1.4) install grade 

````
https://gradle.org/releases/

```
(cmd:yönetici olarak çalıştır) 
"mkdir "c:\Program Files"\gradle"
unzip gradle-6.5-bin.zip and put "c:\Program Files\gradle\" directory

PATH
 
 /"Sistem değişkenleri"/path/düzenle/yeni/
  add "c:\Program Files\gradle\bin"
echo %PATH:;=&echo.%
gradle --version
```

## 1.5) install android and commands tools

```
https://developer.android.com/studio/index.html#command-tools
https://developer.android.com/studio/index.html
https://developer.android.com/studio/command-line/adb // andrid studio devoloper komutlari // adb version
```

### 1.5.1) android path

```
PATH
  /"sistem değişkenleri"/yeni/	
  değişken adı: "ANDROID_HOME"
  değişken değeri: "C:\Users\emr\AppData\Local\Android\Sdk"
  
  /"sistem değişkenleri"/yeni/
	değişken adı: "ANDROID_SDK_ROOT"
  değişken değeri: "C:\Users\emr\AppData\Local\Android\Sdk"

  /"sistem değişkenleri"/path/düzenle/yeni/ 
  add "%ANDROID_HOME%"  
  add "%ANDROID_SDK_ROOT%"
  add "C:\Users\EMR\AppData\Local\Android\Sdk\cmdline-tools\8.0\bin"

(cmd) echo %ANDROID_HOME%
(cmd) echo %ANDROID_SDK_ROOT%
```

sdkmanager packets installation.

```
C:\Users\EMR\AppData\Local\Android\Sdk\cmdline-tools\8.0\bin\
sdkmanager --sdk_root=${ANDROID_HOME} "platform-tools" 
sdkmanager --sdk_root=${ANDROID_HOME} "platforms;android-33"
sdkmanager --sdk_root=${ANDROID_HOME} "tools"
sdkmanager --sdk_root=${ANDROID_HOME} "build-tools;33.0.3"

sdkmanager --sdk_root=${ANDROID_HOME} --version  
sdkmanager --sdk_root=${ANDROID_HOME} --licenses
sdkmanager --sdk_root=${ANDROID_HOME} --list|more
```

## 1.6) install cordova 


first installation.
```
go to  Window PowerShell (with "Run as Administrator")
set-executionpolicy remotesigned // enter (y) for security requirement; remove it later
npm install -g cordova   // global installation
cordova --version  //  12.0.0 (cordova-lib@12.0.1)
node --version  // v20.10.0
npm --version  // 10.2.3
```

proje create
```
cordova create app com.xyz.app app   // proje adi app olur
cd app
npm i
cordova requirements // gereksinimleri gösterir.
//
Requirements check results for android:
Java JDK: installed 11.0.17
Android SDK: installed true
Gradle: installed C:\Program Files\gradle-7.6\bin\gradle.BAT
//
cordova platform add android --save  // android paketi yükle
cordova platform add browser
cordova run browser  // see: http://localhost:8000/index.html
cordova run android

```

other commands and files
```
more package.json //  
more config.xml // ayarlar
-- example 
<?xml version='1.0' encoding='utf-8'?>
<widget android-versionCode="10034" id="emr.ilcin.pwStore" version="1.0.34" xmlns="http://www.w3.org/ns/widgets" xmlns:cdv="http://cordova.apache.org/ns/1.0">
    <name>pwStore</name>
    <description>
        This code stores passwords 
    </description>
    <author email="emr@uludag.edu.tr" href="http://proje1.ilcin.name.tr/ilcine.gitlab.io/public/#/">
        Emrullah İLÇİN
    </author>
    <license>MIT</license>
    <keywords>password,store,manager,create,save</keywords>
    <content src="index.html" />
    <access origin="*" />
    <access origin="cdvfile://*" />
    <access origin="file:///*" />
    <allow-intent href="http://*/*" />
    <allow-intent href="https://*/*" />
    <allow-intent href="tel:*" />
    <allow-intent href="sms:*" />
    <allow-intent href="mailto:*" />
    <allow-intent href="geo:*" />
    <platform name="android">
        <allow-intent href="market:*" />
        <icon density="ldpi" src="www/frontIcon/emr01.ldpi.png" />
        <icon density="mdpi" src="www/frontIcon/emr02.mdpi.png" />
        <icon density="hdpi" src="www/frontIcon/emr03.hdpi.png" />
        <icon density="xhdpi" src="www/frontIcon/emr04.xhdpi.png" />
        <icon density="xxhdpi" src="www/frontIcon/emr05.xxhdpi.png" />
        <icon density="xxxhdpi" src="www/frontIcon/emr06.xxxhdpi.png" />
        <preference name="AndroidXEnabled" value="true" />
        <preference name="AndroidPersistentFileLocation" value="Internal" />
        <preference name="AndroidExtraFilesystems" value="files,files-external,documents,sdcard,cache,cache-external,assets,download,root" />
        <preference name="android-targetSdkVersion" value="33" />
        <preference name="android-compileSdkVersion" value="33" />
        <preference name="android-minSdkVersion" value="24" />
        <preference name="AndroidWindowSplashScreenAnimatedIcon" value="www/frontIcon/keySvg/key_katmanli6.xml" />
        <preference name="AndroidWindowSplashScreenBackground" value="#04AA6D" />
        <preference name="AndroidInsecureFileModeEnabled" value="true" />
    </platform>
    <plugin name="cordova-plugin-exit" source="npm" spec="~1.0.2" />
    <config-file parent="/*" target="AndroidManifest.xml" xmlns:android="http://schemas.android.com/apk/res/android">
        <uses-permission android:maxSdkVersion="32" android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
        <uses-permission android:maxSdkVersion="32" android:name="android.permission.READ_EXTERNAL_STORAGE" />
        <uses-permission android:maxSdkVersion="32" android:name="android.permission.READ_MEDIA_*" />
    </config-file>
    <platform name="ios">
        <allow-intent href="itms:*" />
        <allow-intent href="itms-apps:*" />
    </platform>
</widget>

```

create key.svg
```
sudo apt-get install imagemagick -y
convert key.svg -resize 36x36     emr01.ldpi.png
convert key.svg -resize 48x48     emr02.mdpi.png
convert key.svg -resize 72x72     emr03.hdpi.png
convert key.svg -resize 96x96     emr04.xhdpi.png
convert key.svg -resize 144x144 emr05.xxhdpi.png
convert key.svg -resize 192x192 emr06.xxxhdpi.png
convert key.svg -resize 36x36     emr01.ldpi.png
```

create key
```
keytool -genkey -v -keystore android.keystore -alias android.keystore -keyalg RSA -validity 20000

*create "build.json"

{
    "android": {
        "debug": {
            "keystore": "android.keystore",
            "storePassword": "???",
            "alias": "android.keystore",
            "password" : "???",
            "keystoreType": "",
            "packageType": "apk"
        },
        "release": {
            "keystore": "android.keystore",
            "storePassword": "???",
            "alias": "android.keystore",
            "password" : "???",
            "keystoreType": "",
            "packageType": "bundle"
        }
    }
}
```

create apk and aab
```
1) Create aab  (Android App Bundle)
 cordova build android --release --buildConfig=build.json" 
 
2) Create apk 
 cordova build android --debug --buildConfig="build.json"   // --verbose   ile detay alınır
```


## 1.7)  TELEFON A AKTARMA İŞLEMİ
```
android telefon için   /ayarlar/telefon hakkında/yazılım bilgileri/yapım numaras( 7 kez tıkla)//  

---> developer tools(geliştiriçi seçenekleri)  açılacak  // 

---> geliştirici seçenekleri // usb hata ayıklamayı etkin yap

```




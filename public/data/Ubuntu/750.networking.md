#networking

## ipv4 and ipv6 set

/etc/network/interfaces

```
auto lo
iface lo inet loopback

# The primary network interface
auto eth0

## iface IPV4 static configuration
iface eth0 inet static
        address 94.177.187.77
        netmask 255.255.255.0
        gateway 94.177.187.1
        dns-nameservers 8.8.8.8 8.8.4.4

### Start IPV6 static configuration
iface eth0 inet6 static
        address 2a00:6d40:0060:744d:0000:0000:0000:0077
        netmask 64
        gateway fe80::1
        dns-nameservers 2001:4860:4860::8888 2001:4860:4860::8844
```

## Start

`/etc/init.d/networking restart`

> Usage: /etc/init.d/networking {start|stop|reload|restart|force-reload}

## Test

`ping6 -c 5 2001:4860:4860::8888`

## ipv6 disable
`
```
sudo sysctl -w net.ipv6.conf.all.disable_ipv6 = 1
sysctl -w net.ipv6.conf.default.disable_ipv6 = 1

sysctl -p  # apply change
```

## Ubuntu 18.04 

1. edit: `/etc/netplan/01-netcfg.yaml`

```
network:
  version: 2
  renderer: networkd
  ethernets:
    eth0:
      dhcp4: no
      # IP address/subnet mask
      addresses: [
            94.177.187.77/24,
            "2a00:6d40:0060:744d:0000:0000:0000:0077/64"
            ]
      # default gateway
      gateway4: 94.177.187.1
      gateway6: "fe80::1"
      nameservers:
        # name server this host refers
        addresses: [8.8.8.8,8.8.4.4]
      dhcp6: no
```

2. run: `netplan apply`   
3. see: `ip addr`

> yaratmak için: netplan generate

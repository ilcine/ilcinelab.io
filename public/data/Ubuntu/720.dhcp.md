# Dhcp Server

ip dağıtma sistemi kurulumu

_Emrullah İLÇİN_


## Install

apt-get -y install isc-dhcp-server

## Config

## 1. Hangi interface dağıtacak tanımla eth0, eth1, etc

edit `/etc/default/isc-dhcp-server`

INTERFACES="eth0"

## 2. ipv6 dağıtılmayacaksa disable et. 

`sudo sh -c 'echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6'`

## 3. config.

edit `/etc/dhcp/dhcpd.conf`

```bash
 
# have support for DDNS.)
ddns-update-style none;

# option definitions common to all supported networks...
option domain-name "ilcin.name.tr";
option domain-name-servers 8.8.8.8;

default-lease-time 600;
max-lease-time 7200;

# If this DHCP server is the official DHCP server for the local
# network, the authoritative directive should be uncommented.
authoritative;

# Use this to send dhcp log messages to a different log file (you also
# have to hack syslog.conf to complete the redirection).
log-facility local7;

shared-network ilcinegrup {

# ------- active interface network  --
subnet 94.177.187.0 netmask 255.255.255.0 {
}

# 192.168.1.0 networkunu dağıt.
subnet 192.168.1.0 netmask 255.255.255.0  {
option routers 192.168.1.1;
#option bolge-adi "Network1";
pool {
      #failover peer "dhcp";
      #deny dynamic bootp clients;
      range  192.168.1.110 192.168.1.254;
      }
  }
}
```

## Start or restart

`systemctl restart isc-dhcp-server`

## dağıtılan ip leri gör.

`cat /var/lib/dhcp/dhcpd.leases`



# Apache Proxy

## Forward port "3000" to port "80" //ex: nuxt.js

1) apache: /etc/apache2/sites-available/virtual.host.conf
```
<VirtualHost *:80>
    ServerName www.our-domaine.com
    DocumentRoot /var/www/OUR-DIRECTORY-NAME
    #ProxyRequests on # gerekebilir 
    # has Nuxt runs on port 3000
    ProxyPass / http://localhost:3000/
    ProxyPassReverse / http://localhost:3000/
</VirtualHost>
```

2) sudo a2enmode proxy proxy_http

3) sudo systemctl reload apache2


## EX Links

* <a href="https://stackoverflow.com/questions/56549677/deploy-a-universal-app-made-with-nuxtjs-on-apache-shared-hosting" target="_blank">Link 1</a> 
* <a href="https://dev.to/thomas_ph35/how-i-deployed-my-nuxt-app-on-a-rasberry-pi-3apl " target="_blank">Link 2</a> 

 
# Apache Virtual Hosts

> _Emrullah İLÇİN_

## Example 
Defining domains received from ISP and virtual host in Apache

    ilcin.name.tr  # Domain
    proje1.ilcin.name.tr # Host
    $HOME/proje1 # project directory; ex: Laravel project directory

## Make a definition

`sudo vi /etc/apache2/sites-available/virtual.host.conf` # write the following to this file

```
<VirtualHost *:80>
    ServerName proje1.ilcin.name.tr
    ServerAdmin webmaster@virtual.host
      DocumentRoot /home/emr/proje1/public
        <Directory /home/emr/proje1/public>
            AllowOverride All
            Options FollowSymLinks
            Require all granted
        </Directory>
    ErrorLog /var/log/apache2/virtual.host.error.log
    CustomLog /var/log/apache2/virtual.host.access.log combined
    LogLevel warn
</VirtualHost>
```

* `sudo a2ensite virtual.host`  # make it active; for ineffectiveness use `a2dissite virtual.host` # The definition can also be made to a different file. for this: `sudo vi /etc/apache2/sites-available/proje1.ilcin.name.tr.conf`, <small><i>important; filename must end with .conf </small></i> and run `sudo a2ensite proje1.ilcin.name.tr`

* `sudo systemctl restart apache2` # run services.

* `proje1.ilcin.name.tr` # Define from ISP or your own DNS server
 
## Test

* `echo "Virtual Host Test Ok." > $HOME/proje1/public/test.html`  # create simple html file; and see  in browser `http://proje1.ilcin.name.tr/test.html` # ex: for laravel public directory; default file is`index.html` or `index.php` # If you write to index.html file, use  in url `http://proje1.ilcin.name.tr` # See in browser. 

* Error log: `tail -f /var/log/apache2/virtual.host.error.log` 
* Access log: `tail -f /var/log/apache2/virtual.host.access.log` 

* `apachectl configtest` # apache config test.
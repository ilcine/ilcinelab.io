# Using dummy email account in local post

Yerel postada kukla email hesabı kullanma

_Emrullah İLÇİN_

## Install

`sudo apt-get install msmtp ca-certificates`

## Using as home user  (home user olarak kullanım)
 
* Create and edit this file  "~/.msmtprc"

```
account gmail		
	tls on
	tls_trust_file /etc/ssl/certs/ca-certificates.crt
	protocol smtp
	host smtp.gmail.com
	port 587
	auth login
	# Sender // Gönderen 
	user my_email@gmail.com
	password XXXX
	from my_email@gmail.com
	logfile ~/.msmtp.log
account Xmail
	tls on
	tls_certcheck off
	protocol smtp
	auth login
	host smtp.xyz.com.tr
	port 587
	user ilcine@xyz.com.tr
	from ilcine@xyz.com.tr
	password XXXX  
	logfile ~/.msmtp.log
	
  # If you don't use any "-a" parameter in your command line, the default account "gmail" will be used. 
  # msmtp de -a parametresi kullanmassan default account gmail olur.
  
account default: gmail

```

* `chmod 600 ~/.msmtprc`

* From Gmail; post emr@abc.edu.tr

echo -e "Subject: Test Mail" | msmtp --debug --from=default -t emr@abc.edu.tr`

* From Xmail ; post ilcine@abc.com.tr

`echo -e "Subject: Test Mail" | msmtp -a Xmail -t ilcine@abc.com.tr`

> `msmtp --help`;  Help seçeneği ile diğer parametreler görülür.

* Other Send

Create email.txt

```
To: send@gmail.com
From: my_email@gmail.com
Subject: Email Test using File
Hi,
This is my file email.
```

`cat email.txt | msmtp -a default recipient@gmail.com`

## mail send with php command line (Php cli ile mail gönder)

* edit php.ini. Ex: `/etc/php/7.1/cli/php.ini`

```
sendmail_path = "/usr/bin/msmtp -C /home/emr/.msmtprc -a default -t"
```

* Create phpmail.php
```
<?php
mail ('emr@abc.edu.tr', 'Test Subject', 'Test body text');
exit();
?>
```

* Post mail:

```
php -e phpmail.php
```

* Log:

```
tail ~/.msmtp.log
```

## mail send with apache2  (Apache2 ile gönder.)

* create and edit `/etc/msmtprc`

```
account gmail  
        tls on
        tls_starttls on
        tls_trust_file /etc/ssl/certs/ca-certificates.crt
        protocol smtp
        host smtp.gmail.com
        port 587
        #auth login
        auth on
        user my_email@gmail.com
        password XXXX
        from my_email@gmail.com
        syslog LOG_MAIL
account Xmail
        tls on
        tls_certcheck off
        protocol smtp
        auth login
        host smtp.abc.com.tr
        port 587
        user ilcine@abc.com.tr
        from ilcine@abc.com.tr
        password XXX
        syslog LOG_MAIL
account default: gmail
```

* Change owner and mod:

```
sudo chown www-data:www-data /etc/msmtprc
sudo chmod 600 /etc/msmtprc
```

* Edit php.ini. Ex:  `/etc/php/7.1/apache2/php.ini`

```
sendmail_path = "/usr/bin/msmtp -C /etc/msmtprc -a default -t"
```

* Restart apache:

```
/etc/init.d/apache2 restart
```

* Create `/var/www/html/phpmail.php`

```
<?php
$to = "emr@abc.edu.tr";
$subject = "The Most Beautiful";

// compose headers
$headers = "From: my_email@gmail.com\r\n";
$headers .= "Reply-To: my_email@gmail.com\r\n";
$headers .= "X-Mailer: PHP/".phpversion();

// compose message
$message = "The most beautiful sea:\n";
$message .= "         hasn't been crossed yet.\n";
$message .= "The most beautiful child:\n";
$message .= "         hasn't grown up yet.\n";
$message .= "Our most beautiful days:\n";
$message .= "         we haven't seen yet.\n";
$message .= "And the most beautiful words I wanted to tell you\n";
$message .= "         I haven't said yet...\n";
$message .= "\n";
$message .= "Nazım Hikmet\n";
$message = wordwrap($message, 70);

// send email
if (   mail($to, $subject, $message, $headers)   ) {
    echo "Mail Sent";
    } else {
    echo "Not Sent";
    }
?>
```

* Post mail:

```
http://ilcin.name.tr/phpmail.php
```

* Log:

```
tail -f /var/log/apache2/access.log
tail -f /var/log/apache2/error.log

tail -f /var/log/syslog
```

## Mail post attachment file with php on Apache2

```php

<?php
 
ini_set( 'display_errors', 1 );
error_reporting( E_ALL ^ E_NOTICE); 

function mail_attachment($filename, $path, $mailto, $from_mail, $from_name, $replyto, $subject, $message)
{
    $file = $path . $filename;
    $file_size = filesize($file);
    $handle = fopen($file, "r");
    $content = fread($handle, $file_size);
    fclose($handle);
    $content = chunk_split(base64_encode($content));
    $uid = md5(uniqid(time()));
	$phpversion = phpversion();
   
	$header = "From: " . $from_name . " <" . $from_mail . ">" . PHP_EOL;
	$header .= "Reply-To: " . $replyto . PHP_EOL;
	$header .= "X-Mailer: PHP/" . phpversion() . PHP_EOL;
	$header .= "MIME-Version: 1.0" . PHP_EOL;
	
	$header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"" . PHP_EOL;
	$header .= "--" . $uid . PHP_EOL;
	$header .= "Content-Type: multipart/alternative; boundary=\"". $uid . "\"" . PHP_EOL;

	$msj = "--" . $uid . PHP_EOL;
	$msj .= "Content-Type: text/plain; charset='UTF-8'" . PHP_EOL;
	$msj .= "Content-Transfer-Encoding: quoted-printable" . PHP_EOL;
	$msj .= $message . PHP_EOL;

	$msj .= "--" . $uid . PHP_EOL;
	$msj .= "Content-Type: application/octet-stream; name=\"" . $filename . "\""  . PHP_EOL;
	$msj .= "Content-Transfer-Encoding: base64" . PHP_EOL;
	$msj .= "Content-Disposition: attachment; filename=\"" . $filename . "\"" . PHP_EOL;
	$msj .= $content . PHP_EOL;
    $msj .= "--" . $uid . PHP_EOL;

    if (mail($mailto, $subject, $msj, $header)) {
        echo "Mail send to $mailto";
				
    } else {
        echo "Mail NOT send to $emailto";
    }
}

$my_file = "xyz.tar.gz";
$my_path = $_SERVER['DOCUMENT_ROOT'] . "/mailpost/";
$my_name = "My Name";
$my_mail = "my_email@gmail.com";
$my_replyto = "my_email@gmail.com"; // The answer mail comes in here
$my_subject = "This is a mail with attachment.";
$my_message = "Write the content of the message";

$mail_to = "recipent_email@xyz.edu.tr";

mail_attachment($my_file, $my_path, $mail_to, $my_mail, $my_name, $my_replyto, $my_subject, $my_message);

```

## ubuntu console mail client ; mutt 

* install `apt install mutt`

* config edit `~/.muttrc`
```
set sendmail="/usr/bin/msmtp"
set use_from=yes
set realname="My Name"
set from=xxx@gmail.com
set envelope_from=yes
```
* use
`mutt -a abc.tar.bz2 -s "konusu var" -- gidecekemail@gmamil.com  < mesajicerigi.txt`

> -a attachment; -s subject 

> `mutt` email use; console short commands;  s:Save  m:Mail  r:Reply x:Exit and ?:help

> There are also email client programs such as "alpine", "sup", "mailx". # "alpine", "sup", "mailx" gibi e-posta istemci programları da vardır.

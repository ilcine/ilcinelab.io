activeFiles,	title
010_SunucuTanimlari.md,	        Sunucu Tanimlari laravel
011_Homestead.installation.md,	Homestead installation
080_laravel_install.md,	        laravel install
080_LaravelKurulum.md,	        Laravel Kurulum
090_Config_in_env.md,         	Config in .env
090_Config.md,	                Config
100_Structure.md,             	Structure
101_webpack.md,	                Webpack
110_first_page.md,              First page
120_Artisan.md,	                Artisan
130_Laravel_Auth.md,	          Laravel Auth
130_ROUTE.md,	                  ROUTE
140_MODEL.md,	                  MODEL
150_CONTROLLER.md,	            CONTROLLER
160_VIEW.md,	                  VIEW
170_CRUD.md,                  	CRUD
190_Vue.md,	                    Vue
200_VueCRUD.md,	                Vue CRUD
201_Vue_Router.md,	            Vue Router
210_Debug.md,	                  Debug
220_request.md,	                Request
230_validate.md,	              Validate
240_session.md,	                Session
250_passport.md,	              Passport
251_sanctum.md,                 Sanctrum for spa authentication
270_PageNotFound.md,	          Page Not Found
280_invite.md,	                Invite
290_time_date.md,	              time_date
291_Using_the_Double_Underscore_Translation.md,	Using the Double Underscore Translation
292_Creating_Commands_with_PHP_Artisan.md,	    Creating Commands with PHP Artisan
293_How_to_Use_Laravel_Config_Files.md,	        How to Use Laravel Config Files
294_Laravel_Path.md,                           	Laravel Path
300_href_url_get_post.md,	                      href & url get post
310_Laravel_package_development.md,           	Laravel package development
320_DB.eloquent.queries.md,                   DB eloquent queries
321_DB_HasMany_BelongsTo.md,	          DB HasMany BelongsTo
330_select.md,                          select2 link
340_dynamic_components.md,              Dynamic Components
340_dynamic_component_x-dynamic-component.md,   x-dynamic-component
999duzenle.regex.md,	                           duzenle.regex
# Expect programı

Etkileşimli programlarınızla veya kullanıcı etkileşimi gerektiren komut dosyalarıyla konuşurken, betik komutları çalıştırır; Örnek bash script te IPv4 network ağında bulunan switch,hub,router vb aygıtlar üzerinde oluşan verileri alır, expect ile cihaz yönetimleri vb işlemlerde yapılabilinir. 
Ubuntu sistemlere kurmak için `apt install expect` kullan

> _Emrullah İLÇİN_

## cat exp.shema.alcatel

```
#!/usr/bin/expect -f
# Bu örnek şema Alactel swithler içindir; Alcatel switch üzerindeki Config bilgilerini alır bir dosyaya yazar.

# 192.168.0.1 ip sinde bulunan örnek X marka switch semalarını oluşturmak için "autoexpect telnet 192.168.0.1" 
#  komutunu kullan "script.exp" adında şema otomatik olarak oluşacaktır.

set force_conservative 0 ;# set to 1 to force conservative mode even if
;# script wasn't run conservatively originally
if {$force_conservative} {
 set send_slow {1 .1}
     proc send {ignore arg} {
     sleep .1
     exp_send -s -- $arg
     }
 }

set timeout -1
# expect schema ip(0) admin(1) pass(2) degeri alir
set telnet [lindex $argv 0]
set user [lindex $argv 1]
set pass [lindex $argv 2]

spawn telnet $telnet
match_max 100000

expect -exact "login : "
send -- "$user\r"

expect -exact "password : "
send -- "$pass\r"

expect -exact "\r"

expect -exact "> "
send -- "write terminal\r"

expect -exact "> "
send -- "exit\r"
expect eof
```
 
 
## cat  exp.pw

Örnek Alacatel switch in login ve password unu giriniz. Başka marka switcleriniz varsa bunları da tanımını yaparak girebilirsiniz.

```
#!/bin/bash
#ALCUS="admin"
#ALCPW="pass"
```

## cat exp.txt

Taranacak IP adreslerini alt alta giriniz. "192.168.254.2" $ip değişkenine; "test-3": $ad değişkenine; "alc3": $semaal değişkenine atanır.

`192.168.254.2 test-3 alc3`
 
## cat exp.sh
 
```
#!/bin/bash
# Bu kod switch config lerini toplar; her gün çalıştırılacaksa crontab a yazılır.
# crontab a .. yaz her gun saat 1 de yedek als
# 00 1 * * * bash /home/ag/expect/exp.sh
# autoexpect telnet ip ile config yap "script.exp" dosya oluşsun.

tarih=$(date +%Y-%m-%d)
home="home/ag/expect"
source "/home/ag/expect/exp.pw" &>/dev/null

#-- Gunluk config dizini varmi kontrol eden bolum
if [ ! -d "/home/ag/expect/exp.data/$tarih/" ];
    then
    mkdir "/home/ag/expect/exp.data/$tarih"
fi

alc3="exp.shema.alcatel"

IFS=""; # IFS(input field separator)
cat /$home/exp.txt |\

#--while döngo başı----------------------------------------------------------------------------
while read -r i; do
    ip=$(echo $i |cut -d" " -f1)
    ad=$(echo $i |cut -d" " -f2)
    semaal=$(echo $i |cut -d" " -f3)
    
    if [ $semaal = "alc3" ]; then sema=$alc3; lo=$alcus; pw=$alcpw; fi
    
    echo " $ip $ad "
    
    #echo "expect /$home/$sema $ip $lo $pw > /$home/exp.data/$tarih/$ad$ip.txt"
    expect /$home/$sema $ip $lo $pw > /$home/exp.data/$tarih/$ad$ip.txt

done

```

## Çalıştırmak için 

`bash exp.sh`


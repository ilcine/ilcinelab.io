# Alcatel swith config notları

## Command line de oturum süresi

`session timeout cli 120`

## Http de oturum süresi

`session timeout http 120`

## Device name prompt

`session prompt default "My Switch ->"`

## System Name

`system name My_Switch`

## Cihaz açılış banner ı

`vi /flash/switch/pre_banner.txt`

## Değişikliklerin yazılması 


```
write memory
copy working certified
exit
```

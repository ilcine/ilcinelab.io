# ipv4

| Class   | Prefix | Mask            | Wildcard Mask   | IP              |
|---------|--------|-----------------|-----------------|-----------------|
| Class A | 8      | 255.0.0.0       | 0.0.0.255       | 256 * 256 * 256 |
| Class A | 9      | 255.128.0.0     | 0.0.128.255     | 256 * 256 * 128 |
| Class A | 10     | 255.192.0.0     | 0.0.192.255     | 256 * 256 * 64  |
| Class A | 11     | 255.224.0.0     | 0.0.224.255     | 256 * 256 * 32  |
| Class A | 12     | 255.240.0.0     | 0.0.240.255     | 256 * 256 * 16  |
| Class A | 13     | 255.248.0.0     | 0.0.248.255     | 256 * 256 * 8   |
| Class A | 14     | 255.252.0.0     | 0.0.252.255     | 256 * 256 * 4   |
| Class A | 15     | 255.254.0.0     | 0.0.254.255     | 256 * 256 * 2   |
| Class B | 16     | 255.255.0.0     | 0.0.255.255     | 256 * 256     |
| Class B | 17     | 255.255.128.0   | 0.128.255.255   | 256 * 128     |
| Class B | 18     | 255.255.192.0   | 0.192.255.255   | 256 * 64      |
| Class B | 19     | 255.255.224.0   | 0.224.255.255   | 256 * 32      |
| Class B | 20     | 255.255.240.0   | 0.240.255.255   | 256 * 16      |
| Class B | 21     | 255.255.248.0   | 0.248.255.255   | 256 * 8       |
| Class B | 22     | 255.255.252.0   | 0.252.255.255   | 256 * 4       |
| Class B | 23     | 255.255.254.0   | 0.254.255.255   | 256 * 2       |
| Class C | 24     | 255.255.255.0   | 0.255.255.255   | 256          |
| Class C | 25     | 255.255.255.128 | 128.255.255.255 | 128          |
| Class C | 26     | 255.255.255.192 | 192.255.255.255 | 64           |
| Class C | 27     | 255.255.255.224 | 224.255.255.255 | 32           |
| Class C | 28     | 255.255.255.240 | 240.255.255.255 | 16           |
| Class C | 29     | 255.255.255.248 | 248.255.255.255 | 8            |
| Class C | 30     | 255.255.255.252 | 252.255.255.255 | 4            |
| Class C | 31     | 255.255.255.254 | 254.255.255.255 | 2            |
| Class C | 32     | 255.255.255.255 | 255.255.255.255 | 1            |

# Private IPv4 address spaces 

|IP address range	              | subnet mask	                 | 	classful description           |
|-------------------------------|------------------------------|---------------------------------|
|10.0.0.0 – 10.255.255.255	    | 10.0.0.0/8 (255.0.0.0)	     |	single class A network         |
|172.16.0.0 – 172.31.255.255	  | 172.16.0.0/12 (255.240.0.0)	 | 	16 contiguous class B networks |
|192.168.0.0 – 192.168.255.255	| 192.168.0.0/16 (255.255.0.0) |	256 contiguous class C networks|


# ilcine.gitlab.io

## <a href="https://gitlab.com/ilcine/linuxnotlari/">Linux Notları</a>
```
These Commands are also used with Linux versions of Debian, Ubuntu, Redhat, Centos, Fedora,
Suse, Slackware and Unix systems with minor modifications.
```
```
Bu Komutlar Linux sürümleri ile Debian, Ubuntu, Redhat, Centos, Fedora,
Suse, Slackware ve küçük değişikliklerle Unix sistemler de de kullanılır.
```

## <a href="https://gitlab.com/ilcine/UbuntuNotlari/">Ubuntu Notları</a>
```
Ubuntu is an open source and free operating system developed on the Linux kernel. These notes have been tested on Ubuntu 16.04.
```
```
Ubuntu, Linux çekirdeği üzerinde geliştirilen açık kaynak kodlu ve ücretsiz bir işletim sistemidir. Bu notlar Ubuntu 16.04 üzerinde denenmiştir.
```
## <a href="https://gitlab.com/ilcine/laravelnotlari/">Laravel Notları</a>
```
Laravel, MVC yapısına sahip açık kaynak kodlu PHP frameworküdür. Nesne Yönelimli Programlama (OOP) özelliği vardır.
Kurulum için Bağımlılık yöneticisi olan Composer kullanır.
```
```
Laravel is an open source PHP framework with MVC structure. Object Oriented Programming (OOP) feature is available.
It uses Composer, which is a Dependency manager for setup.
```
## <a href="https://gitlab.com/ilcine/VueNotlari/">Vue Notları</a>
```
VueJS is a framework that uses javaScript features. Provides coding for front-end programmers with interactive Web content.
Immediately detects changes in CSS or HTML and applies the page without refresh.
```
```
VueJS, javaScript özellikleri kullanan bir çerçevedir.
Interaktif Web içeriği olan front-end(Ön yüz) programcılarına kodlama sunar.
Css'de veya HTML'de yapılan değişiklikleri hemen algılar sayfayı refresh yapmadan uygular.
```
## <a href="https://gitlab.com/ilcine/NetworkingNotlari/">Networking Notları</a>
```
Computer networks or data networks share digital resources.
These data connections are based on wired or wireless environments.
In these notes, there are computer softwares that use the "tcp/ip" network protocol.
```

```
Bilgisayan networleri veya veri ağları digital kaynakları paylaşır.
Bu veri bağlantıları kablolu veya kablosuz ortamlar üzerine kurulur.
Bu notlarda, "tcp/ip" network protokolu kullanan bilgisayar yazılımları vardır.
```
## <a href="https://gitlab.com/ilcine/AccountingNotlari/">Genel Muhasebe Projesi</a>
```
This General Accounting project has been used for 4 years
in a company for transactions such as day-book, Trial balance, Ledger, Budget, Period On / Off etc.
The code is written on Linux Slackware 10.2 with the programming language "Progress v9".
New versions of Progress can be applied by modifying "Progress | OpenEdge".
```
```
Bu Genel Muhasebe projesi Yevmiye, Mizan, Defteri Kebir, Bütçe, Dönem Açma/Kapama vb
işlemleri için bir firmada 4 yıl boyunca kullanılmıştır.
Kod "Progress v9" programlama dili ile Linux Slackware 10.2 üzerinde yazılmıştır.
Progress in yeni sürümleri "Progress|OpenEdge" de değişiklik yapılarak uygulanabilir.
```
